# SENBIR: A game about computers?
Senbir is a game.  Of sorts.  About virtual computers.



# RISC-06: Tiny Processing
Bundled here with the Senbir source code is a C++ library for the fictional RISC-06 processor architecture, based off of Senbir's own TC-06.
More details in ./RISC06.

One day to be included in Senbir itself as an alterante computer type you can select.  Will be forked at some point to make a TC-06 library for ALL THE PROCESSING SPEED

## GENERAL DISCLAIMER
All this RISC-06 stuff is HIGHLY work in progress, almost certainly bug-filled, and written by a C++ novice.  It's my first time writing anything big with QT, let alone distributing it, so I might be making all sorts of mistakes, in which case, message me in some form or another.

## RISC-06 IDE: Self-contained assembler, VM, and debug tools
Bundled here with the Senbir source code is also a QT-5 (5.5, to be exact) IDE/Assembler/VM/Debugger for the RISC-06 library.  See ./QT/Risc06Implementation/ - you should be able to open it with the latest version of QT Creator and build it.

# YAPA-02: Yet Another Processor Architecture
Like the RISC-06, the YAPA-02 C++ library is another VM library for a virtual machine that'll be featured in Senbir.  It's actually...really quite good, and incredibly usable.  More details to come.  At some point.  See also ./YAPA02 for more, like in ./YAPA02/src/yapa02.cpp - plenty of comment data there.

## GENERAL DISCLAIMER
The YAPA-02 code is INCREDIBLY work-in-progress, as it's being designed with interop functionality in mind.  This means it IS working in Unity (some pictures can be found in a devlog at the Itch page for Senbir - https://cliffracerx.itch.io/senbir - you're looking for "Slow Progress", in which there will be pictures of laptops covered in text) - there are unsafe uses of pointers, no checks for if RAM access will be out of bounds in either direction, etc.  Be prepared for your implementation programs to suffer from frequent segmentation faults until I iron out the bugs.
Do NOT use it in a shipping application that needs to be stable.  Things will violently explode.
