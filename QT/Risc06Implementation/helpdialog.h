#ifndef HELPDIALOG_H
#define HELPDIALOG_H

#include <QDialog>
#include <QTreeWidgetItem>
#include <QFile>
#include <QTextStream>
#include <QDebug>
#include <QTextBrowser>

namespace Ui {
class HelpDialog;
}

class HelpDialog : public QDialog
{
    Q_OBJECT

public:
    explicit HelpDialog(QWidget *parent = 0);
    ~HelpDialog();
    Ui::HelpDialog *ui;
    QTextBrowser* GetText();

private slots:
    void on_HelpTopics_itemChanged(QTreeWidgetItem *item, int column);

    void on_HelpTopics_itemClicked(QTreeWidgetItem *item, int column);

private:

    void LoadHTML(const char* path);
};

#endif // HELPDIALOG_H
