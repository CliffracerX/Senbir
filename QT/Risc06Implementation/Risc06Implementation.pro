#-------------------------------------------------
#
# Project created by QtCreator 2018-10-16T17:24:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Risc06Implementation
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    risc06highlighter.cpp \
    optionsdialog.cpp \
    helpdialog.cpp \
    aboutdialog.cpp

HEADERS  += mainwindow.h \
    risc06highlighter.h \
    optionsdialog.h \
    helpdialog.h \
    aboutdialog.h

FORMS    += mainwindow.ui \
    optionsdialog.ui \
    helpdialog.ui \
    aboutdialog.ui
INCLUDEPATH += /home/cliffracerx/LD42/LD42RunningOutOfSpace/RISC06/src/
LIBS += /home/cliffracerx/LD42/LD42RunningOutOfSpace/RISC06/bin/shared/librisc06.so

DISTFILES +=

RESOURCES += \
    resources.qrc
