#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QPixmap>
#include <QTimer>
#include "risc06.h"
#include "optionsdialog.h"
#include "helpdialog.h"
#include "aboutdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionQuit_triggered();

    void on_buttonPower_clicked();

    void on_checkExecute_stateChanged(int arg1);
    void doTick(); //why oh why couldn't we have Connects with a manual passing of arguments?
    void doTick(bool manualOverride, int nTicks);

    void on_buttonCompile_clicked();

    void PasteRom();

    void on_buttonCompileDrive_clicked();

    uint8_t* DoCompile(int size);

    void on_buttonTickManual_clicked();

    void on_actionExtended_Mode_triggered();

    void on_actionDefault_Mode_triggered();

    void on_actionSettings_triggered();

    void on_actionROM_triggered();

    void on_actionDRIVE_triggered();

    void on_actionROM_Run_triggered();

    void on_actionDRIVE_Run_triggered();

    void on_actionRISC_06_Manual_triggered();

    void on_actionAbout_triggered();

    void on_actionNew_triggered();

    void on_actionSave_Assembly_triggered();

    void on_actionLoad_Assembly_triggered();

    void on_actionSave_As_triggered();

    void CloseTab(int index);
    void TabChange(int index);

    void on_actionClose_File_triggered();

    void on_actionDefault_w_RAM_triggered();

private:
    Ui::MainWindow *ui;
    QLabel *pwrLabel;
    QPixmap iconOff,iconOn,iconPause;
    CompState myComputer;
    bool powerOn;
    QTimer *timer;
    uint8_t* compiled;
    void TogglePower(bool power);
    QColor black;
    int numTicks;
    int myMode;
    void InitMode(int mode);
    void InitIco();
    OptionsDialog* oDiag;
    HelpDialog* hDiag;
    AboutDialog* aDiag;
    QTabBar* tabs;
    QList<QString>* files;
    QList<QString>* names;
};

#endif // MAINWINDOW_H
