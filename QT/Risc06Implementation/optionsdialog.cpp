#include "optionsdialog.h"
#include "ui_optionsdialog.h"
#include <QColorDialog>
#include <QPainter>
#include "mainwindow.h"
#include <QSettings>

OptionsDialog::OptionsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::OptionsDialog)
{
    ui->setupUi(this);
}

OptionsDialog::~OptionsDialog()
{
    delete ui;
}

QPixmap OptionsDialog::ColorSwatch(QColor col)
{
    QPixmap pixmap(QIcon(":/images/colorSwatch.png").pixmap(25, 25));
    QPainter painterOn(&pixmap);
    painterOn.setCompositionMode(QPainter::CompositionMode_Multiply);
    painterOn.fillRect(pixmap.rect(), col);
    painterOn.setCompositionMode(QPainter::CompositionMode_DestinationIn);
    painterOn.fillRect(pixmap.rect(), pixmap);
    painterOn.end();
    return pixmap;
}

void OptionsDialog::InitColors()
{
    ui->lblAsmBG->setPixmap(ColorSwatch(asmBG));
    ui->lblAsmFG->setPixmap(ColorSwatch(asmFG));
    ui->lblAsmNum->setPixmap(ColorSwatch(asmNum));
    ui->lblAsmOpC->setPixmap(ColorSwatch(asmOpC));
    ui->lblEMTOff->setPixmap(ColorSwatch(mon0));
    ui->lblEMTOn1->setPixmap(ColorSwatch(mon1));
    ui->lblEMTOn2->setPixmap(ColorSwatch(mon2));
    ui->lblEMTOn3->setPixmap(ColorSwatch(mon3));
}

void OptionsDialog::on_btnAsmBG_clicked()
{
    asmBG = QColorDialog::getColor(asmBG, this);
    ui->lblAsmBG->setPixmap(ColorSwatch(asmBG));
}

void OptionsDialog::on_btnAsmFG_clicked()
{
    asmFG = QColorDialog::getColor(asmFG, this);
    ui->lblAsmFG->setPixmap(ColorSwatch(asmFG));
}

void OptionsDialog::on_btnAsmNum_clicked()
{
    asmNum = QColorDialog::getColor(asmNum, this);
    ui->lblAsmNum->setPixmap(ColorSwatch(asmNum));
}

void OptionsDialog::on_btnAsmOpC_clicked()
{
    asmOpC = QColorDialog::getColor(asmOpC, this);
    ui->lblAsmOpC->setPixmap(ColorSwatch(asmOpC));
}

void OptionsDialog::on_btnEMTOff_clicked()
{
    mon0 = QColorDialog::getColor(mon0, this);
    ui->lblEMTOff->setPixmap(ColorSwatch(mon0));
}

void OptionsDialog::on_btnEMTOn1_clicked()
{
    mon1 = QColorDialog::getColor(mon1, this);
    ui->lblEMTOn1->setPixmap(ColorSwatch(mon1));
}

void OptionsDialog::on_btnEMTOn2_clicked()
{
    mon2 = QColorDialog::getColor(mon2, this);
    ui->lblEMTOn2->setPixmap(ColorSwatch(mon2));
}

void OptionsDialog::on_btnEMTOn3_clicked()
{
    mon3 = QColorDialog::getColor(mon3, this);
    ui->lblEMTOn3->setPixmap(ColorSwatch(mon3));
}

void OptionsDialog::on_btnAsmCM_clicked()
{
    asmCM = QColorDialog::getColor(asmCM, this);
    ui->lblAsmCM->setPixmap(ColorSwatch(asmCM));
}

void OptionsDialog::on_buttonBox_accepted()
{
    this->refreshThings = true;
    this->SaveSettings();
}

void OptionsDialog::LoadSettings()
{
    QSettings settings("The 006 Cooperative", "RISC-06 Emulator");
    /*this->asmBG = QColor(settings.value("ColorAsmBG-R", this->asmBG.red()).toInt(), settings.value("ColorAsmBG-R", this->asmBG.green()).toInt(),settings.value("ColorAsmBG-B", this->asmBG.blue()).toInt());
    this->asmFG = QColor(settings.value("ColorAsmFG-R", this->asmFG.red()).toInt(), settings.value("ColorAsmFG-R", this->asmFG.green()).toInt(),settings.value("ColorAsmFG-B", this->asmFG.blue()).toInt());
    this->asmNum = QColor(settings.value("ColorAsmNum-R", this->asmNum.red()).toInt(), settings.value("ColorAsmNum-R", this->asmNum.green()).toInt(),settings.value("ColorAsmNum-B", this->asmNum.blue()).toInt());
    this->asmOpC = QColor(settings.value("ColorAsmOpC-R", this->asmOpC.red()).toInt(), settings.value("ColorAsmOpC-R", this->asmOpC.green()).toInt(),settings.value("ColorAsmOpC-B", this->asmOpC.blue()).toInt());
    this->asmCM = QColor(settings.value("ColorAsmCM-R", this->asmCM.red()).toInt(), settings.value("ColorAsmCM-R", this->asmCM.green()).toInt(),settings.value("ColorAsmCM-B", this->asmCM.blue()).toInt());*/
    this->asmBG = settings.value("Assembler/BackgroundColor", this->asmBG).value<QColor>();
    this->asmFG = settings.value("Assembler/ForegroundColor", this->asmFG).value<QColor>();
    this->asmNum = settings.value("Assembler/NumberColor", this->asmNum).value<QColor>();
    this->asmOpC = settings.value("Assembler/OpCodeColor", this->asmOpC).value<QColor>();
    this->asmCM = settings.value("Assembler/CommentColor", this->asmCM).value<QColor>();
    this->mon0 = settings.value("Customization/MonitorColor0", this->mon0).value<QColor>();
    this->mon1 = settings.value("Customization/MonitorColor1", this->mon1).value<QColor>();
    this->mon2 = settings.value("Customization/MonitorColor2", this->mon2).value<QColor>();
    this->mon3 = settings.value("Customization/MonitorColor3", this->mon3).value<QColor>();
    this->winSize = settings.value("Window/Size", this->winSize).value<QSize>();
    this->winPos = settings.value("Window/Position", this->winPos).value<QPoint>();
    this->winMaximized = settings.value("Window/Maximized", this->winMaximized).toBool();
    settings.sync();
}

void OptionsDialog::SaveSettings()
{
    QSettings settings("The 006 Cooperative", "RISC-06 Emulator");
    /*settings.setValue("ColorAsmBG-R", this->asmBG.red());
    settings.setValue("ColorAsmBG-G", this->asmBG.green());
    settings.setValue("ColorAsmBG-B", this->asmBG.blue());
    settings.setValue("ColorAsmFG-R", this->asmFG.red());
    settings.setValue("ColorAsmFG-G", this->asmFG.green());
    settings.setValue("ColorAsmFG-B", this->asmFG.blue());
    settings.setValue("ColorAsmNum-R", this->asmNum.red());
    settings.setValue("ColorAsmNum-G", this->asmNum.green());
    settings.setValue("ColorAsmNum-B", this->asmNum.blue());
    settings.setValue("ColorAsmOpC-R", this->asmOpC.red());
    settings.setValue("ColorAsmOpC-G", this->asmOpC.green());
    settings.setValue("ColorAsmOpC-B", this->asmOpC.blue());
    settings.setValue("ColorAsmCM-R", this->asmCM.red());
    settings.setValue("ColorAsmCM-G", this->asmCM.green());
    settings.setValue("ColorAsmCM-B", this->asmCM.blue());*/
    settings.setValue("Assembler/BackgroundColor", this->asmBG);
    settings.setValue("Assembler/ForegroundColor", this->asmFG);
    settings.setValue("Assembler/NumberColor", this->asmNum);
    settings.setValue("Assembler/OpCodeColor", this->asmOpC);
    settings.setValue("Assembler/CommentColor", this->asmCM);
    settings.setValue("Customization/MonitorColor0", this->mon0);
    settings.setValue("Customization/MonitorColor1", this->mon1);
    settings.setValue("Customization/MonitorColor2", this->mon2);
    settings.setValue("Customization/MonitorColor3", this->mon3);
    settings.setValue("Window/Size", this->winSize);
    settings.setValue("Window/Position", this->winPos);
    settings.setValue("Window/Maximized", this->winMaximized);
    settings.sync();
}

void OptionsDialog::on_buttonBox_rejected()
{
    this->refreshThings = true;
    this->LoadSettings();
    this->InitColors();
}
