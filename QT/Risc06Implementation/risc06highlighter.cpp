#include "risc06highlighter.h"
#include <QSyntaxHighlighter>
#include <QRegExp>
#include <QPalette>

Risc06Highlighter::Risc06Highlighter(QTextDocument *parent) : QSyntaxHighlighter(parent)
{

}

void Risc06Highlighter::highlightBlock(const QString &text)
{
    QTextCharFormat myClassFormat;
    //Numbers
    myClassFormat.setFontWeight(QFont::Light);
    myClassFormat.setForeground(myWin->asmNum);
    int i = 0;
    QRegExp expression1("\\d");
    while ((i = expression1.indexIn(text, i)) != -1)
    {
      setFormat(i, expression1.matchedLength(), myClassFormat);
      i+=expression1.matchedLength();
    }
    //Op-codes
    myClassFormat.setFontWeight(QFont::Bold);
    myClassFormat.setForeground(myWin->asmOpC);
    QRegExp expression2("\\b(NLS|DTC|NIL|HLT|JMP|MOV|DAT|OPR|BRN|SPC|nls|dtc|nil|hlt|jmp|mov|dat|opr|brn|spc)\\b");
    i = 0;
    while ((i = expression2.indexIn(text, i)) != -1)
    {
      setFormat(i, expression2.matchedLength(), myClassFormat);
      i+=expression2.matchedLength();
    }
    //Comments
    myClassFormat.setFontWeight(QFont::Normal);
    myClassFormat.setForeground(myWin->asmCM);
    QRegExp expression0("//.+");
    i = 0;
    while ((i = expression0.indexIn(text, i)) != -1)
    {
      setFormat(i, expression0.matchedLength(), myClassFormat);
      i+=expression0.matchedLength();
    }
}
