﻿#ifndef RISC06HIGHLIGHTER_H
#define RISC06HIGHLIGHTER_H
#include <QSyntaxHighlighter>
#include <QRegExp>
#include <QPalette>
#include <optionsdialog.h>

class Risc06Highlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    Risc06Highlighter(QTextDocument *parent);
    OptionsDialog* myWin;
protected:
    void highlightBlock(const QString &text);
private:
    QPalette myPalette;
};

#endif // RISC06HIGHLIGHTER_H
