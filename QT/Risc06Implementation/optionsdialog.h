#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QDialog>

namespace Ui {
class OptionsDialog;
}

class OptionsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit OptionsDialog(QWidget *parent = 0);
    ~OptionsDialog();
    QColor asmBG,asmFG,asmNum,asmOpC,asmCM,mon0,mon1,mon2,mon3;
    QPixmap ColorSwatch(QColor col);
    void InitColors();
    bool refreshThings = true;
    void LoadSettings();
    void SaveSettings();
    QSize winSize;
    QPoint winPos;
    bool winMaximized;

private slots:
    void on_btnAsmBG_clicked();

    void on_btnAsmFG_clicked();

    void on_btnAsmNum_clicked();

    void on_btnAsmOpC_clicked();

    void on_btnEMTOff_clicked();

    void on_btnEMTOn1_clicked();

    void on_btnEMTOn2_clicked();

    void on_btnEMTOn3_clicked();

    void on_btnAsmCM_clicked();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::OptionsDialog *ui;
};

#endif // OPTIONSDIALOG_H
