#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "risc06.h"
#include "risc06highlighter.h"
#include <QPalette>
#include <QTimer>
#include <QPainter>
#include <qglobal.h>
#include <QFileDialog>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QFontDatabase::addApplicationFont(":/monospace.ttf");
    QFont myFont = QFont("Monospace");
    ui->assemblerText->setFont(myFont);
    ui->listRam->setFont(myFont);
    powerOn = false;
    //Code for status bar inspired by /src/qt/bitcoingui.cpp as seen in the original release of Tulpacoin.
    //If you're curious to see an old-school joke cryptocoin, pop over to https://dev-tulpacoin.pantheonsite.io/ or https://github.com/CliffracerX/tulpacoin-client to grab a copy.
    //If you live in Windows, you're basically out of luck.  Compiling for Windows is hell.
    //...I wonder if you could make a Tulpacoin client for the RISC-06 or TC-06?  ~CliffracerX
    QFrame *frameBlocks = new QFrame();
    frameBlocks->setContentsMargins(0,0,0,0);
    frameBlocks->setMinimumWidth(73);
    frameBlocks->setMaximumWidth(73);
    QHBoxLayout *frameBlocksLayout = new QHBoxLayout(frameBlocks);
    frameBlocksLayout->setContentsMargins(3,0,3,0);
    frameBlocksLayout->setSpacing(3);
    pwrLabel = new QLabel();
    //printf(offCol.alpha);
    //printf(onCol.alpha);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(doTick()));
    black = QColor("black");
    //trying to debug monitor rendering
    /*QImage img = QPixmap(16, 8).toImage();
    img.setPixel(1, 1, 0xff0000);
    img.setPixel(1, 2, 0xff0000);
    img.setPixel(1, 3, 0xff0000);
    img.setPixel(2, 4, 0xff0000);
    img.setPixel(2, 5, 0xff0000);
    img.setPixel(2, 6, 0xff0000);
    img.setPixel(3, 3, 0xff0000);
    img.setPixel(3, 4, 0xff0000);
    img.setPixel(4, 4, 0xff0000);
    img.setPixel(4, 5, 0xff0000);
    img.setPixel(4, 6, 0xff0000);
    img.setPixel(5, 1, 0xff0000);
    img.setPixel(5, 2, 0xff0000);
    img.setPixel(5, 3, 0xff0000);
    ui->monitor->setPixmap(QPixmap::fromImage(img.scaled(320, 160)));*/
    oDiag = new OptionsDialog();
    oDiag->asmBG = this->palette().base().color();
    oDiag->asmFG = this->palette().text().color();
    oDiag->asmNum = this->palette().brightText().color();
    oDiag->asmOpC = this->palette().highlight().color();
    oDiag->asmCM = this->palette().mid().color();
    oDiag->mon0 = QColor("black");
    oDiag->mon1 = this->palette().highlight().color();
    oDiag->mon2 = QColor("grey");
    oDiag->mon3 = QColor("white");
    oDiag->winSize = this->size();
    oDiag->winPos = this->pos();
    oDiag->winMaximized = this->isMaximized();
    oDiag->LoadSettings();
    oDiag->InitColors();
    InitMode(0);
    Risc06Highlighter* r06HL = new Risc06Highlighter(ui->assemblerText->document());
    r06HL->myWin = oDiag;
    InitIco();
    frameBlocksLayout->addStretch();
    frameBlocksLayout->addWidget(pwrLabel);
    statusBar()->addPermanentWidget(frameBlocks);
    hDiag = new HelpDialog();
    aDiag = new AboutDialog();
    Risc06Highlighter* r06HL2 = new Risc06Highlighter(hDiag->GetText()->document());
    r06HL2->myWin = oDiag;
    this->tabs = new QTabBar(ui->assemblerTabs);
    this->tabs->setTabsClosable(true);
    this->tabs->addTab("new*");
    //this is such a hack
    ui->assemblerTabs->layout()->removeWidget(ui->assemblerText);
    ui->assemblerTabs->layout()->addWidget(this->tabs);
    ui->assemblerTabs->layout()->addWidget(ui->assemblerText);
    this->files = new QList<QString>();
    this->files->append("");
    this->names = new QList<QString>();
    this->names->append("new*");
    connect(this->tabs, SIGNAL(tabCloseRequested(int)), this, SLOT(CloseTab(int)));
    connect(this->tabs, SIGNAL(currentChanged(int)), this, SLOT(TabChange(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
    oDiag->SaveSettings();
    delete oDiag;
    delete hDiag;
    delete aDiag;
}

void MainWindow::CloseTab(int index)
{
    if(this->tabs->count()>1)
    {
        this->tabs->removeTab(index);
        this->files->removeAt(index);
        this->names->removeAt(index);
        this->TabChange(tabs->currentIndex());
    }
    else
    {
        this->tabs->setTabText(0, "new*");
        this->files->replace(0, "");
        this->names->replace(0, "new*");
        ui->assemblerText->setText("");
    }
}

void MainWindow::TabChange(int index)
{
    ui->assemblerText->setText(this->files->at(index));
}

void MainWindow::InitIco()
{
    QPixmap pixmap(QIcon(":/images/pwrIcon.png").pixmap(16, 16));
    iconOn = pixmap;
    QPainter painterOn(&iconOn);
    painterOn.setCompositionMode(QPainter::CompositionMode_Multiply);
    painterOn.fillRect(iconOn.rect(), oDiag->mon1);
    painterOn.setCompositionMode(QPainter::CompositionMode_DestinationIn);
    painterOn.fillRect(iconOn.rect(), pixmap);
    painterOn.end();
    iconOff = pixmap;
    QPainter painterOff(&iconOff);
    painterOff.setCompositionMode(QPainter::CompositionMode_Multiply);
    painterOff.fillRect(iconOff.rect(), oDiag->mon0);
    painterOff.setCompositionMode(QPainter::CompositionMode_DestinationIn);
    painterOff.fillRect(iconOff.rect(), pixmap);
    painterOff.end();
    pwrLabel->setPixmap(iconOff);
    iconPause = pixmap;
    QPainter painterPause(&iconPause);
    painterPause.setCompositionMode(QPainter::CompositionMode_Multiply);
    painterPause.fillRect(iconPause.rect(), oDiag->mon2);
    painterPause.setCompositionMode(QPainter::CompositionMode_DestinationIn);
    painterPause.fillRect(iconPause.rect(), pixmap);
    painterPause.end();
}

void MainWindow::InitMode(int mode)
{
    if(timer->isActive())
    {
        timer->stop();
    }
    if(ui->listRam->count()>0)
    {
        ui->listRam->clear();
    }
    if(mode == 0) //DEFAULT MODE
    {
        timer->start(25);
        CompState cs = {0, false, 0, 0, 0, 0, new unsigned char[32], 32, new unsigned char[128], 128, 0, new unsigned char[256+2], 256+2, 1, new unsigned char[1], 1, 2, new unsigned char[1], 1, 3};
        myComputer = cs;
        for(int x = 0; x<16; x++)
        {
            for(int y = 0; y<8; y++)
            {
                myComputer.per0Data[x+(y*16)] = 0;
            }
        }
        for(int i = 0; i<myComputer.ramLen; i++)
        {
            ui->listRam->addItem("00000000");
        }
        for(int i = 0; i<myComputer.per1DataLen; i++)
        {
            myComputer.per1Data[i] = 0;
        }
        numTicks = 1;
        myMode = 0;
    }
    else if(mode == 1) //EXTENDED MODE: MORE RAM, FANCIER MONITOR, BETTER DRIVE
    {
        timer->start(10);
        CompState cs = {0, false, 0, 0, 0, 0, new unsigned char[256], 256, new unsigned char[16384+2], 16384+2, 4, new unsigned char[65536+2], 65536+2, 3, new unsigned char[1], 1, 2, new unsigned char[1], 1, 3};
        myComputer = cs;
        for(int x = 0; x<128; x++)
        {
            for(int y = 0; y<128; y++)
            {
                myComputer.per0Data[(x+(y*128))+2] = 0;
            }
        }
        for(int i = 0; i<myComputer.ramLen; i++)
        {
            ui->listRam->addItem("00000000");
        }
        for(int i = 0; i<myComputer.per1DataLen; i++)
        {
            myComputer.per1Data[i] = 0;
        }
        numTicks = 4;
        myMode = 1;
    }
    else if(mode == 2) //DEFAULT MODE W/ RAM EXTENSION
    {
        timer->start(25);
        CompState cs = {0, false, 0, 0, 0, 0, new unsigned char[64], 64, new unsigned char[128], 128, 0, new unsigned char[256+2], 256+2, 1, new unsigned char[1], 1, 2, new unsigned char[1], 1, 3};
        myComputer = cs;
        for(int x = 0; x<16; x++)
        {
            for(int y = 0; y<8; y++)
            {
                myComputer.per0Data[x+(y*16)] = 0;
            }
        }
        for(int i = 0; i<myComputer.ramLen; i++)
        {
            ui->listRam->addItem("00000000");
        }
        for(int i = 0; i<myComputer.per1DataLen; i++)
        {
            myComputer.per1Data[i] = 0;
        }
        numTicks = 1;
        myMode = 0;
    }
}

void MainWindow::doTick()
{
    this->doTick(false, numTicks);
}

void MainWindow::doTick(bool manualOverride, int nTicks)
{
    this->files->replace(this->tabs->currentIndex(), ui->assemblerText->toPlainText());
    if(oDiag->refreshThings)
    {
        QPalette tempPal = QPalette(ui->assemblerText->palette());
        tempPal.setColor(QPalette::Base, oDiag->asmBG);
        tempPal.setColor(QPalette::Text, oDiag->asmFG);
        ui->assemblerText->setPalette(tempPal);
        hDiag->GetText()->setPalette(tempPal);
        InitIco();
        this->size().setHeight(oDiag->winSize.height());
        this->size().setWidth(oDiag->winSize.width());
        this->pos().setX(oDiag->winPos.x());
        this->pos().setY(oDiag->winPos.y());
        if(oDiag->winMaximized)
            this->showMaximized();
        oDiag->refreshThings = false;
    }
    //ui->assemblerText->palette()
    if((ui->checkExecute->isChecked() || manualOverride) && powerOn)
    {
        myComputer = compute(nTicks, myComputer.pointer, myComputer.halted, myComputer.haltedTicks, myComputer.offset, myComputer.ram, myComputer.ramLen, myComputer.reg0, myComputer.reg1, myComputer.per0Data, myComputer.per0DataLen, myComputer.per0Type, myComputer.per1Data, myComputer.per1DataLen, myComputer.per1Type, myComputer.per2Data, myComputer.per2DataLen, myComputer.per2Type, myComputer.per3Data, myComputer.per3DataLen, myComputer.per3Type);
        ui->listRam->setCurrentRow(myComputer.pointer);
    }
    ui->labelCounter->setText("COUNTER: " + QString::number(myComputer.pointer));
    //img.setPixel(0, 0, 0xff0000);
    if(myMode == 0) //default monitor
    {
        QImage img = QPixmap(16, 8).toImage();
        for(int x = 0; x<16; x++)
        {
            for(int y = 0; y<8; y++)
            {
                QRgb col = oDiag->mon0.rgb();
                if(myComputer.per0Data[x+(y*16)] >> 7 & 0x1==1)
                {
                    col = oDiag->mon1.rgb();
                }
                img.setPixel(x, y, col);
            }
        }
        ui->monitor->setPixmap(QPixmap::fromImage(img.scaled(320, 160)));
    }
    else if(myMode == 1) //extended monitor
    {
        QImage img = QPixmap(128, 128).toImage();
        for(int x = 0; x<128; x++)
        {
            for(int y = 0; y<128; y++)
            {
                QRgb col = oDiag->mon0.rgb();
                uint8_t mCol = myComputer.per0Data[(x+(y*128))+2];
                if(mCol == 1)
                {
                    col = oDiag->mon1.rgb();
                }
                else if(mCol == 2)
                {
                    col = oDiag->mon2.rgb();
                }
                else if(mCol == 3)
                {
                    col = oDiag->mon3.rgb();
                }
                img.setPixel(x, y, col);
            }
        }
        ui->monitor->setPixmap(QPixmap::fromImage(img.scaled(256, 256)));
    }
    for(int i = 0; i<myComputer.ramLen; i++)
    {
        ui->listRam->item(i)->setText(QString::number(myComputer.ram[i], 2).rightJustified(8, '0'));
    }
    ui->labelRamView->setText("^RAM^, R0 = "+QString::number(myComputer.reg0)+", R1 = "+QString::number(myComputer.reg1));
}

void MainWindow::on_actionQuit_triggered()
{
    QCoreApplication::quit();
}

void MainWindow::TogglePower(bool power)
{
    if(!power)
    {
        pwrLabel->setPixmap(iconOff);
        ui->buttonPower->setText("POWER: OFF");
        for(int i = 0; i<myComputer.ramLen; i++)
        {
            myComputer.ram[i] = 0;
        }
        if(myMode == 0)
        {
            for(int x = 0; x<16; x++)
            {
                for(int y = 0; y<8; y++)
                {
                    myComputer.per0Data[x+(y*16)] = 0;
                }
            }
        }
        else if(myMode == 1)
        {
            for(int x = 0; x<128; x++)
            {
                for(int y = 0; y<128; y++)
                {
                    myComputer.per0Data[(x+(y*128))+2] = 0;
                }
            }
        }
        myComputer.pointer = 0;
        myComputer.halted = false;
        myComputer.haltedTicks = 0;
        myComputer.reg0 = 0;
        myComputer.reg1 = 0;
    }
    else
    {
        if(ui->checkExecute->isChecked())
        {
            pwrLabel->setPixmap(iconOn);
        }
        else
        {
            pwrLabel->setPixmap(iconPause);
        }
        ui->buttonPower->setText("POWER: ON");
        this->PasteRom();
    }
}

void MainWindow::on_buttonPower_clicked()
{
    powerOn = !powerOn;
    TogglePower(powerOn);
}

void MainWindow::on_checkExecute_stateChanged(int arg1)
{
    TogglePower(powerOn);
}

void MainWindow::on_buttonCompile_clicked()
{
    compiled = this->DoCompile(myComputer.ramLen);
}

uint8_t* MainWindow::DoCompile(int size)
{
    uint8_t* localCompiled = new uint8_t[size];
    QStringList split = ui->assemblerText->toPlainText().split('\n');
    for(int i = 0; i<split.length(); i++)
    {
        QString localSTR = "";
        bool ok = false;
        if(split[i].startsWith("DTC", Qt::CaseInsensitive)) //NIL (N/A) <byte of data>
        {
            QStringList vars = split[i].split(' ');
            localSTR = vars[1];
            localCompiled[i] = localSTR.toInt(&ok, 2);
        }
        if(split[i].startsWith("NIL", Qt::CaseInsensitive)) //NIL (000) 00000
        {
            localSTR += "00000000";
            localCompiled[i] = localSTR.toInt(&ok, 2);
        }
        if(split[i].startsWith("HLT", Qt::CaseInsensitive)) //HLT (001) <5-bits # of ticks>
        {
            localSTR += "001";
            QStringList vars = split[i].split(' ');
            if(vars.length()>1)
            {
                QString ticks = QString::number(vars[1].toInt(&ok), 2).rightJustified(5, '0');
                ticks.remove(ticks.length()-5); //based on a drastic assumption that QT will format binary as high->low (e.g, 0001 = 1, 1000 = 8)
                localSTR += ticks;
            }
            else
            {
                localSTR += "00000";
            }
            //end result: 001TTTTT, where the Ts represent ticks.
            localCompiled[i] = localSTR.toInt(&ok, 2);
        }
        if(split[i].startsWith("JMP", Qt::CaseInsensitive)) //MOV (010) <1-bit flag> <4-bit distance>
        {
            localSTR += "010";
            QStringList vars = split[i].split(' ');
            QString flag = QString::number(vars[1].toInt(&ok), 2).rightJustified(1, '0');
            flag = flag.remove(flag.length()-1); //based on a drastic assumption that QT will format binary as high->low (e.g, 0001 = 1, 1000 = 8)
            localSTR += flag;
            QString distance = QString::number(vars[2].toInt(&ok)-1, 2).rightJustified(4, '0');
            distance = distance.remove(distance.length()-4);
            localSTR += distance;
            //end result: 010FDDDD, where the Ds represent the distance, and F is the flag.
            localCompiled[i] = localSTR.toInt(&ok, 2);
        }
        if(split[i].startsWith("MOV", Qt::CaseInsensitive)) //MOV (011) <1-bit flag> <1-bit offset> <3-bit address>
        {
            localSTR += "011";
            QStringList vars = split[i].split(' ');
            QString flag = QString::number(vars[1].toInt(&ok), 2).rightJustified(1, '0');
            flag.remove(flag.length()-1); //based on a drastic assumption that QT will format binary as high->low (e.g, 0001 = 1, 1000 = 8)
            localSTR += flag;
            QString reg = QString::number(vars[2].toInt(&ok), 2).rightJustified(1, '0');
            reg.remove(reg.length()-1);
            localSTR += reg;
            QString addr = QString::number(vars[3].toInt(&ok), 2).rightJustified(3, '0');
            addr.remove(addr.length()-3);
            localSTR += addr;
            //end result: 011FDDDD, where the Ds represent the distance, and F is the flag.
            localCompiled[i] = localSTR.toInt(&ok, 2);
        }
        if(split[i].startsWith("DAT", Qt::CaseInsensitive)) //DAT (100) <1-bit GET/SET flag> <2-bit peripheral ID> <bit argument 1> <bit argument 2>
        {
            localSTR += "100";
            QStringList vars = split[i].split(' ');
            QString flag = QString::number(vars[1].toInt(&ok), 2).rightJustified(1, '0');
            flag.remove(flag.length()-1); //based on a drastic assumption that QT will format binary as high->low (e.g, 0001 = 1, 1000 = 8)
            localSTR += flag;
            QString peripheral = QString::number(vars[2].toInt(&ok), 2).rightJustified(2, '0');
            peripheral.remove(peripheral.length()-2);
            localSTR += peripheral;
            QString bitA = QString::number(vars[3].toInt(&ok), 2).rightJustified(1, '0');
            bitA.remove(bitA.length()-1);
            localSTR += bitA;
            QString bitB = QString::number(vars[4].toInt(&ok), 2).rightJustified(1, '0');
            bitB.remove(bitB.length()-1);
            localSTR += bitB;
            //end result: 010FDDDD, where the Ds represent the distance, and F is the flag.
            localCompiled[i] = localSTR.toInt(&ok, 2);
        }
        if(split[i].startsWith("OPR", Qt::CaseInsensitive)) //OPR (101) <4-bit operation> <1-bit flag>
        {
            localSTR += "101";
            QStringList vars = split[i].split(' ');
            QString oprCode = QString::number(vars[1].toInt(&ok), 2).rightJustified(4, '0');
            oprCode.remove(oprCode.length()-4); //based on a drastic assumption that QT will format binary as high->low (e.g, 0001 = 1, 1000 = 8)
            localSTR += oprCode;
            QString flag = QString::number(vars[2].toInt(&ok), 2).rightJustified(1, '0');
            flag.remove(flag.length()-1);
            localSTR += flag;
            //end result: 101OOOOF, where the Os represent the operation, and F is the flag.
            localCompiled[i] = localSTR.toInt(&ok, 2);
        }
        if(split[i].startsWith("BRN", Qt::CaseInsensitive)) //BRN (110) <2-bit operation> <3-bit offset>
        {
            localSTR += "110";
            QStringList vars = split[i].split(' ');
            QString op = QString::number(vars[1].toInt(&ok), 2).rightJustified(2, '0');
            op.remove(op.length()-2); //based on a drastic assumption that QT will format binary as high->low (e.g, 0001 = 1, 1000 = 8)
            localSTR += op;
            QString addr = QString::number(vars[2].toInt(&ok)-1, 2).rightJustified(3, '0');
            addr.remove(addr.length()-3);
            localSTR += addr;
            //end result: 101OOOOF, where the Os represent the operation, and F is the flag.
            localCompiled[i] = localSTR.toInt(&ok, 2);
        }
        if(split[i].startsWith("SPC", Qt::CaseInsensitive)) //SPC (111) <3-bit start bit> <2-bit length>
        {
            localSTR += "111";
            QStringList vars = split[i].split(' ');
            QString startBit = QString::number(vars[1].toInt(&ok), 2).rightJustified(3, '0');
            startBit.remove(startBit.length()-3); //based on a drastic assumption that QT will format binary as high->low (e.g, 0001 = 1, 1000 = 8)
            localSTR += startBit;
            int tempV = vars[2].toInt(&ok)/2;
            tempV -= 1;
            if(tempV < 0)
                tempV = 0;
            QString bitLength = QString::number(tempV, 2).rightJustified(2, '0');
            bitLength.remove(bitLength.length()-2);
            localSTR += bitLength;
            //end result: 101OOOOF, where the Os represent the operation, and F is the flag.
            localCompiled[i] = localSTR.toInt(&ok, 2);
        }
    }
    return localCompiled;
}

void MainWindow::PasteRom()
{
    for(int i = 0; i<myComputer.ramLen; i++)
    {
        myComputer.ram[i] = compiled[i];
    }
}

void MainWindow::on_buttonCompileDrive_clicked()
{
    uint8_t* tempCompiled = this->DoCompile(myComputer.per1DataLen-2);
    for(int i = 0; i<myComputer.per1DataLen-2; i++)
    {
        myComputer.per1Data[i+2] = tempCompiled[i];
    }
}

void MainWindow::on_buttonTickManual_clicked()
{
    this->doTick(true, 1);
}

void MainWindow::on_actionExtended_Mode_triggered()
{
    InitMode(1);
}

void MainWindow::on_actionDefault_Mode_triggered()
{
    InitMode(0);
}

void MainWindow::on_actionSettings_triggered()
{
    oDiag->show();
}

void MainWindow::on_actionROM_triggered()
{
    this->on_buttonCompile_clicked();
}

void MainWindow::on_actionDRIVE_triggered()
{
    this->on_buttonCompileDrive_clicked();
}

void MainWindow::on_actionROM_Run_triggered()
{
    this->on_buttonCompile_clicked();
    this->TogglePower(false);
    this->TogglePower(true);
    this->powerOn = true;
}

void MainWindow::on_actionDRIVE_Run_triggered()
{
    this->on_buttonCompileDrive_clicked();
    this->TogglePower(false);
    this->TogglePower(true);
    this->powerOn = true;
}

void MainWindow::on_actionRISC_06_Manual_triggered()
{
    hDiag->show();
}

void MainWindow::on_actionAbout_triggered()
{
    aDiag->show();
}

void MainWindow::on_actionNew_triggered()
{
    int index = this->tabs->addTab("new*");
    this->files->append("");
    this->names->append("new*");
    this->tabs->setCurrentIndex(index);
}

void MainWindow::on_actionSave_Assembly_triggered()
{
    QString saveLoc = "";
    if(this->names->at(this->tabs->currentIndex())=="new*")
    {
        saveLoc = QFileDialog::getSaveFileName(this, "Save File", QDir::currentPath()+"/new.rasm", "RISC-06 Assembly (*.rasm)");
        this->names->replace(this->tabs->currentIndex(), saveLoc);
        QStringList splits = saveLoc.split("/");
        QString tempName = splits[splits.length()-1];
        this->tabs->setTabText(this->tabs->currentIndex(), tempName);
    }
    else //otherwise, assume the name's already set
    {
        saveLoc = this->tabs->tabText(this->tabs->currentIndex());
    }
    QFile file(saveLoc);
    if(!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::information(this, "Unable to open file", file.errorString());
        return;
    }
    else
    {
        QTextStream out(&file);
        out << files->at(this->tabs->currentIndex()).toUtf8();
    }
}

void MainWindow::on_actionLoad_Assembly_triggered()
{
    QString saveLoc = QFileDialog::getOpenFileName(this, "Open File", QDir::currentPath()+"/", "RISC-06 Assembly (*.rasm)");
    QString readFile = "";
    QFile file(saveLoc);
    if(!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::information(this, "Unable to open file", file.errorString());
        return;
    }
    else
    {
        QTextStream in(&file);
        readFile = in.readAll();
    }
    if(this->names->at(this->tabs->currentIndex())=="new*" && this->files->at(this->tabs->currentIndex())=="")
    {
        this->names->replace(this->tabs->currentIndex(), saveLoc);
        QStringList splits = saveLoc.split("/");
        QString tempName = splits[splits.length()-1];
        this->tabs->setTabText(this->tabs->currentIndex(), tempName);
        this->files->replace(this->tabs->currentIndex(), readFile);
    }
    else
    {
        QStringList splits = saveLoc.split("/");
        QString tempName = splits[splits.length()-1];
        int index = this->tabs->addTab(tempName);
        this->files->append(readFile);
        this->tabs->setCurrentIndex(index);
        this->names->append(saveLoc);
    }
    ui->assemblerText->setText(readFile);
}

void MainWindow::on_actionSave_As_triggered()
{
    QString saveLoc = QFileDialog::getSaveFileName(this, "Save File", QDir::currentPath()+"/new.rasm", "RISC-06 Assembly (*.rasm)");
    this->names->replace(this->tabs->currentIndex(), saveLoc);
    QStringList splits = saveLoc.split("/");
    QString tempName = splits[splits.length()-1];
    this->tabs->setTabText(this->tabs->currentIndex(), tempName);
    QFile file(saveLoc);
    if(!file.open(QIODevice::WriteOnly))
    {
        QMessageBox::information(this, "Unable to open file", file.errorString());
        return;
    }
    else
    {
        QTextStream out(&file);
        out << files->at(this->tabs->currentIndex()).toUtf8();
    }
}

void MainWindow::on_actionClose_File_triggered()
{
    CloseTab(this->tabs->currentIndex());
}

void MainWindow::on_actionDefault_w_RAM_triggered()
{
    this->InitMode(2);
}
