#include "helpdialog.h"
#include "ui_helpdialog.h"

HelpDialog::HelpDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HelpDialog)
{
    ui->setupUi(this);
    LoadHTML(":/HelpScreens/HelpOverview.html");
}

HelpDialog::~HelpDialog()
{
    delete ui;
}

QTextBrowser* HelpDialog::GetText()
{
    return ui->HelpText;
}

void HelpDialog::on_HelpTopics_itemChanged(QTreeWidgetItem *item, int column)
{
    //QTextStream out(stdout);
    //out << item->text(column) << endl;
    if(item->text(column)=="Overview")
    {
        LoadHTML(":/HelpScreens/HelpOverview.html");
    }
}

void HelpDialog::LoadHTML(const char* path)
{
    QFile file(path);
    file.open(QFile::ReadOnly | QFile::Text);
    QTextStream stream(&file);
    ui->HelpText->setHtml(stream.readAll());
}

void HelpDialog::on_HelpTopics_itemClicked(QTreeWidgetItem *item, int column)
{
    //QTextStream out(stdout);
    //out << item->text(column) << endl;
    if(item->text(column)=="Overview")
    {
        LoadHTML(":/HelpScreens/HelpOverview.html");
    }
    //Overviews
    if(item->text(column)=="The RISC-06 Computer")
    {
        LoadHTML(":/HelpScreens/Risc06Info.html");
    }
    if(item->text(column)=="Assembly Info")
    {
        LoadHTML(":/HelpScreens/AssemblyInfo.html");
    }
    //Modes
    if(item->text(column)=="Default Mode")
    {
        LoadHTML(":/HelpScreens/DefaultOverview.html");
    }
    if(item->text(column)=="Extended Mode")
    {
        LoadHTML(":/HelpScreens/ExtendedOverview.html");
    }
    //OpCodes
    if(item->text(column)=="NLS - NIL List")
    {
        LoadHTML(":/HelpScreens/CodeNLS.html");
    }
    if(item->text(column)=="DTC - DATAC")
    {
        LoadHTML(":/HelpScreens/CodeDTC.html");
    }
    if(item->text(column)=="NIL - 0-byte")
    {
        LoadHTML(":/HelpScreens/CodeNIL.html");
    }
    if(item->text(column)=="HLT - Halt")
    {
        LoadHTML(":/HelpScreens/CodeHLT.html");
    }
    if(item->text(column)=="MOV - Move")
    {
        LoadHTML(":/HelpScreens/CodeMOV.html");
    }
    if(item->text(column)=="JMP - Jump")
    {
        LoadHTML(":/HelpScreens/CodeJMP.html");
    }
    if(item->text(column)=="DAT - Peripherals")
    {
        LoadHTML(":/HelpScreens/CodeDAT.html");
    }
    if(item->text(column)=="OPR - Assorted")
    {
        LoadHTML(":/HelpScreens/CodeOPR.html");
    }
    if(item->text(column)=="BRN - Branch")
    {
        LoadHTML(":/HelpScreens/CodeBRN.html");
    }
    if(item->text(column)=="SPC - Splice")
    {
        LoadHTML(":/HelpScreens/CodeSPC.html");
    }
}
