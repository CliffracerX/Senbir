#include <stdint.h>
#include <cmath>
#include "risc06.h"
struct CompState compute(int numTicks, uint8_t curPointer, bool haltedIn, uint8_t haltedTicksIn, uint8_t offsetIn, unsigned char* ramIn, int ramLen, uint8_t reg0, uint8_t reg1, unsigned char* per0DIn, int per0DLen, uint8_t per0, unsigned char* per1DIn, int per1DLen, uint8_t per1, unsigned char* per2DIn, int per2DLen, uint8_t per2, unsigned char* per3DIn, int per3DLen, uint8_t per3)
{
	uint8_t pointer = curPointer;
	uint8_t* ram = ramIn;
	uint8_t* per0D = per0DIn;
	uint8_t* per1D = per1DIn;
	uint8_t* per2D = per2DIn;
	uint8_t* per3D = per3DIn;
	uint8_t r0 = reg0;
	uint8_t r1 = reg1;
	bool halted = haltedIn;
	uint8_t haltedTicks = haltedTicksIn;
	uint8_t offset = offsetIn;
	for(int i = 0; i<numTicks; i++)
	{
		if(haltedTicks > 0)
		{
			haltedTicks = haltedTicks - 1;
		}
		if(!halted && haltedTicks<=0)
		{
			uint8_t ramVal = ram[pointer];
			uint8_t operation = (ramVal >> 5) & 0x7;
			uint8_t value = ramVal & 0x1F;
			if(operation == 0) //NIL
			{
				pointer += 1;
			}
			else if(operation == 1) //HLT [ticks: 5b]
			{
				if(value==0)
				{
					halted = true;
				}
				else
				{
					haltedTicks = value;
				}
				pointer += 1;
			}
			else if(operation == 2) //JMP <flag: 1b> <offset: 4b>
			{
				uint8_t flag = (value >> 4) & 0x1; //first bit of args
				uint8_t offset = value & 0xF; //last four bits of args
				if(flag == 0)
				{
                                        pointer = pointer + (offset+1);
				}
				else
				{
                                        pointer = pointer - (offset+1);
				}
			}
			else if(operation == 3) //MOV <flag: 1b> <reg: 1b> <address: 3b>
			{
				uint8_t flag = (value >> 4) & 0x1; //first bit of args
				uint8_t reg = (value >> 3) & 0x1; //second bit of args
				uint8_t address = value & 0x7; //last three bits of args
				if(flag == 0) //MOVI
				{
					if(reg == 0)
					{
						r0 = ram[(address+offset)%ramLen];
					}
					else
					{
						r1 = ram[(address+offset)%ramLen];
					}
				}
				else //MOVO
				{
					if(reg == 0)
					{
						ram[(address+offset)%ramLen] = r0;
					}
					else
					{
						ram[(address+offset)%ramLen] = r1;
					}
				}
				pointer += 1;
			}
			else if(operation == 4) //DAT <flag: 1b> <peripheral: 2b> <arg 1: 1b> <arg 2: 2b>
			{
				//PERIPHERAL TYPE 0: MONITOR
				//PERIPHERAL TYPE 1: DISK
				//PERIPHERAL TYPE 2: KEYBOARD
				//PERIPHERAL TYPE 3: EXTENDED DISK
                                //PERIPHERAL TYPE 4: EXTENDED MONITOR
				//Monitor data will be a 1d array of WIDTH*HEIGHT.
				//Disk data will be a 1d array of DISKLEN+2, to account for offsets.
				//Extended disk data will be a 1d array of DISKLEN+3 - data[0] is the block number, data[1] is the offset within that block, and data[2] is current state.
                                //Extended monitor data will be a 1d array of (WIDTH*HEIGHT) + 2, to account for offsets.  Data[0] is the first byte of args, and data[1] is the state.
				uint8_t flag = (value >> 4) & 0x1; //first bit of args
				uint8_t peripheral = (value >> 2) & 0x3; //next two bits of args
				bool arg1 = ((value >> 1) & 0x1) == 1; //4th bit of args
				bool arg2 = (value & 0x1) == 1; //final bit of args
				uint8_t* data;
				uint8_t type;
				if(peripheral==0)
				{
					data = per0D;
					type = per0;
				}
				else if(peripheral==1)
				{
					data = per1D;
					type = per1;
				}
				else if(peripheral==2)
				{
					data = per2D;
					type = per2;
				}
				else if(peripheral==3)
				{
					data = per3D;
					type = per3;
				}
				if(flag == 0) //GETDATA
				{
					uint8_t command;
					uint8_t result;
					if(arg2)
					{
						command = ram[pointer+2];
					}
					else
					{
						if(arg1)
						{
							command = r0;
						}
						else
						{
							command = r1;
						}
					}
					if(type==0) //MONITOR
					{
						//FXXXXYYY
						uint8_t monFlag = (command >> 7) & 0x1; //first bit
						uint8_t monX = (command >> 3) & 0xF; //next four bits
						uint8_t monY = command & 0x7; //last three bits
						result = data[monX+(monY*16)];
					}
					else if(type==1)
					{
						result = data[command+2];
					}
					else if(type==3)
					{
						if(data[0] == 0)
						{
							data[0] = 1;
							data[1] = command;
							result = 0;
						}
						else if(data[0] == 1)
						{
							data[0] = 0;
							data[1] = 0;
							result = data[command+(256*data[1])];
						}
					}
                                        else if(type == 4)
                                        {
                                            //ARG 0: XXXXXXXY
                                            //ARG 1: YYYYYY00 - Y cont'd from arg 0
                                            //RETRN: 000000CC - returns 2-bit color int
                                            if(data[1] == 1)
                                            {
                                                //command is technically equal to 00000000CCCCCCCC
                                                //meanwhile, the shifted data is  CCCCCCCC00000000
                                                //or in binary, command = 0x00XX while data = 0xXX00.
                                                uint16_t cmdT = data[0] << 8;
                                                uint16_t merged = command | cmdT;
                                                uint8_t x = (merged >> 8) & 0x7F;
                                                uint8_t y = (merged >> 1) & 0x7F;
                                                result = data[(x+(y*128))+2];
                                                data[1] = 0;
                                                data[0] = 0;
                                            }
                                            else
                                            {
                                                data[1] = 1;
                                                data[0] = command;
                                                result = 0;
                                            }
                                        }
					if(arg1)
					{
						r1 = result;
					}
					else
					{
						r0 = result;
					}
				}
				else //SETDATA
				{
					uint8_t command;
					if(arg2)
					{
						command = ram[pointer+2];
					}
					else
					{
						if(arg1)
						{
							command = r1;
						}
						else
						{
							command = r0;
						}
					}
					if(type==0) //MONITOR
					{
						//CXXXXYYY
						uint8_t monFlag = (command >> 7) & 0x1; //first bit
						uint8_t monX = (command >> 3) & 0xF; //next four bits
						uint8_t monY = command & 0x7; //last three bits
						data[monX+(monY*16)] = command;
					}
					else if(type==1) //DISK DRIVE
					{
						if(data[0] == 0) //a location has not yet been specified
						{
							data[1] = command;
							data[0] = 1;
						}
						else
						{
							data[data[1]+2] = command;
							data[0] = 0;
							data[1] = 0;
						}
					}
					else if(type==3) //EXTENDED DISK DRIVE
					{
						if(data[0] == 0)
						{
							data[0] = 1;
							data[1] = command;
						}
						else if(data[0] == 1)
						{
							data[0] = 2;
							data[2] = command;
						}
						else if(data[0] == 2)
						{
							data[0] = 0;
							data[data[1]*256 + data[2]] = command;
							data[1] = 0;
							data[2] = 0;
						}
					}
                                        else if(type == 4)
                                        {
                                            //ARG 0: XXXXXXXY
                                            //ARG 1: YYYYYYCC - Y cont'd from arg 0
                                            if(data[1] == 1)
                                            {
                                                //command is technically equal to 00000000CCCCCCCC
                                                //meanwhile, the shifted data is  CCCCCCCC00000000
                                                //or in binary, command = 0x00XX while data = 0xXX00.
                                                uint16_t datT = data[0]; //has X
                                                datT = datT << 8;
                                                datT &= 0xFF00; //mask it
                                                uint16_t cmdT = command;
                                                cmdT &= 0x00FF; //mask it
                                                uint16_t merged = cmdT | datT;
                                                //using base-10 for sanity's sake
                                                uint8_t x = (merged >> 9) & 127;
                                                uint8_t y = (merged >> 2) & 127;
                                                uint8_t color = merged & 3;
                                                data[(x+(y*128))+2] = color;
                                                data[1] = 0;
                                                data[0] = 0;
                                            }
                                            else
                                            {
                                                data[1] = 1;
                                                data[0] = command;
                                            }
                                        }
				}
				pointer += 1;
			}
			else if(operation==5) //OPR <op-code: 4b> <flag: 1b>
			{
				//printf("OP 5");
				uint8_t utilOp = (value >> 1) & 0xf; //first four bits of args
				uint8_t flag = value & 0x1; //last bit of args
				pointer += 1;
				if(utilOp == 0) //add or subtract
				{
					//printf("SUB 0");
					if(flag == 0) //subtract
					{
						r0 = r0-r1;
					}
					else //add
					{
						r0 = r0+r1;
					}
				}
				else if(utilOp == 1) //multiply or divide
				{
					//printf("SUB 1");
					if(flag == 0) //divide
					{
						r0 = r0/r1;
					}
					else //multiply
					{
						r0 = r0*r1;
					}
				}
				else if(utilOp == 2) //copy
				{
					//printf("SUB 2");
					if(flag == 0) //R1 = R0
					{
						r1 = r0;
					}
					else //R0 = R1
					{
						r0 = r1;
					}
				}
				else if(utilOp == 3) //modulo or exponent
				{
					//printf("SUB 3");
					if(flag == 0) //modulo
					{
						r0 = r0%r1;
					}
					else //exponent
					{
						r0 = r0^r1;
					}
				}
				else if(utilOp == 4) //real jump
				{
					//printf("SUB 4");
					if(flag == 0) //from r0
					{
						pointer = r0-1;
					}
					else //from r1
					{
						pointer = r1-1;
					}
				}
				else if(utilOp == 5) //offset
				{
					//printf("SUB 5");
					if(flag == 0) //from r0
					{
						offset = r0;
					}
					else //from program counter
					{
						offset = pointer;
					}
				}
				else if(utilOp == 6) //Set 0 - 1: reg 0
				{
					//printf("SUB 6");
					if(flag == 0) //0
					{
						r0 = 0;
					}
					else //1
					{
						r0 = 1;
					}
				}
				else if(utilOp == 7) //Set 0 - 1: reg 1
				{
					//printf("SUB 7");
					if(flag == 0) //0
					{
						r1 = 0;
					}
					else //1
					{
						r1 = 1;
					}
				}
				else if(utilOp == 8) //Set 2 - 3: reg 0
				{
					//printf("SUB 8");
					if(flag == 0) //2
					{
						r0 = 2;
					}
					else //3
					{
						r0 = 3;
					}
				}
				else if(utilOp == 9) //Set 2 - 3: reg 1
				{
					//printf("SUB 9");
					if(flag == 0) //2
					{
						r1 = 2;
					}
					else //3
					{
						r1 = 3;
					}
				}
                                else if(utilOp == 10) //Shift
				{
					//printf("SUB 10");
                                        uint8_t data,len;
					if(flag == 0) //0
					{
                                                data = r0;
                                                len = r1;
					}
					else //1
					{
                                                data = r1;
                                                len = r0;
					}
                                        for(int i = 0; i<len; i++)
                                        {
                                            data = (data >> 1) | ((data << 7) & 0x80);
                                        }
                                        if(flag == 0)
                                        {
                                            r0 = data;
                                        }
                                        else
                                        {
                                            r1 = data;
                                        }
				}
			}
			else if(operation == 6) //BRN <operation: 2b> <dest. addr: 3b>
			{
				uint8_t opType = (value >> 3) & 0x3; //first two bits of args
				uint8_t dest = value & 0x7; //last three bits of args
				bool opCheck = false;
				if(opType == 0) //equal
				{
					opCheck = r0 == r1;
				}
				else if(opType == 1) //not equal
				{
					opCheck = r0 != r1;
				}
				else if(opType == 2) //R0 > R1
				{
					opCheck = r0 > r1;
				}
				else if(opType == 3) //R0 < R1
				{
					opCheck = r0 < r1;
				}
				if(opCheck)
				{
                                        pointer += 1 + dest;
				}
				else
				{
					pointer += 1;
				}
			}
			else if(operation == 7) //SPC <start point: 3b> <length: 2b>
			{
				uint8_t startP = (value >> 2) & 0x7; //first three bits of args
                                uint8_t length = ((value & 0x3)+1)*2; //last two bits of args
                                //this is the old way of doing it, based off some stuff online.
                                /*uint8_t tempR = r1 & (255 << (startP+length));
                                tempR |= r0 << startP;
                                r1 = tempR | (r1 & ~(255 << startP));*/
                                //this is the 006 way of doing it, very improvised
                                uint8_t maskCopy = pow(2, length)-1;
                                maskCopy = maskCopy << startP;
                                uint8_t maskRemainder = ~maskCopy;
                                uint8_t bitsFromReg = r0 & maskCopy;
                                uint8_t maskedReg = r1 & maskRemainder;
                                r1 = maskedReg | bitsFromReg;
                                //but hey, it works like you'd expect, so...nailed it?
				pointer += 1;
			}
		}
	}
	struct CompState cs = {pointer, halted, haltedTicks, offset, r0, r1, ram, ramLen, per0D, per0DLen, per0, per1D, per1DLen, per1, per2D, per2DLen, per2, per3D, per3DLen, per3};
	return cs;
}
