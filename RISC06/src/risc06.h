#ifndef LIBRISC06_H
#define LIBRISC06_H

#include <stdint.h>
struct CompState
{
	uint8_t pointer;
	bool halted;
	uint8_t haltedTicks;
	uint8_t offset;
	uint8_t reg0;
	uint8_t reg1;
	unsigned char* ram;
	int ramLen;
	unsigned char* per0Data;
	int per0DataLen;
	uint8_t per0Type;
	unsigned char* per1Data;
	int per1DataLen;
	uint8_t per1Type;
	unsigned char* per2Data;
	int per2DataLen;
	uint8_t per2Type;
	unsigned char* per3Data;
	int per3DataLen;
	uint8_t per3Type;
};
struct CompState compute(int numTicks, uint8_t curPointer, bool haltedIn, uint8_t haltedTicksIn, uint8_t offsetIn, unsigned char* ramIn, int ramLen, uint8_t reg0, uint8_t reg1, unsigned char* per0DIn, int per0DLen, uint8_t per0, unsigned char* per1DIn, int per1DLen, uint8_t per1, unsigned char* per2DIn, int per2DLen, uint8_t per2, unsigned char* per3DIn, int per3DLen, uint8_t per3);

#endif //LIBRISC06_H
