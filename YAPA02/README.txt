# YAPA-02 Library
The YAPA-02 is Yet Another Processor Architecture, and easily the most usable I've made yet.

Documentation is in the Zim wiki (or the Documentation folder for compiled HTML), type "make" in a *nixalike to get yourself a build, or muck around with setting up mxe in a *nixalike install for cross-compiling a Windows DLL with "buildMXE.sh".

If you use a GtkSourceView-based editor for your code, you can copy yasm.lang to ~/.local/share/gtksourceview-3.0/language-specs/ or similar to get "Other/YAPA-02 Assembly" added to your filetype list.  It should even auto-format .yasm files you open!