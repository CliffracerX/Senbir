#include <stdint.h>
#include <cmath>
#include <stdio.h>
#include "yapa02.h"
//register 0-PC: Program Counter
//register 1-IA: Instruction Address, use TBD
//register 2-RT: jump ReTurn
//register 3-ID: Interrupt Data
//register 4-IR: Interrupt Return (address)
//register 5-15: General Registers

//Local variable names: we use a constant array to store them so as to avoid blowing out the RAM when performing thousands of cycles a frame.
//Unity ***DIES HORRIFICALLY*** in those circumstances.
//tempVars[0] = opcodeB
//tempVars[1] = opcode
//tempVars[2] = flag
//tempVars32[0] = sourceVal
//tempVars32[1] = destVal
//last 6 of each reserved for findArg and setArg ops.  use varies by op type (e.g, registers need a different set of values than RAM)
//tempVars[9] = loop for RAM view && tLen
//tempVars[10] = argDat
//tempVars[11] = argTyp
//tempVars32[9] = temp counter
//tempVars32[15] = valueToFind
//tempVars[8] = temporary "boolean" to signify if interrupts have been handled yet
extern "C"
{
	void inDirtyRam(uint32_t* array, uint32_t* tempVars32)
	{
		for(tempVars32[9] = 1; tempVars32[9]<array[0]+1; tempVars32[9]++)
		{
			if(array[tempVars32[9]]==tempVars32[9])
			{
				return;
			}
		}
		tempVars32[9] = -1;
	}
	
	uint32_t yapa02FindArg(uint32_t* registers, uint8_t* ram, uint32_t ramLen, uint32_t* tempVars32, uint8_t* tempVars)
	{
		tempVars[10] = ram[registers[1]];
		tempVars[11] = (tempVars[10] >> 6) & 0x3; 				//first 2 bits specify argument type
		if(tempVars[11] == 0)							//type 0: register
		{
			//fprintf(stderr, "findArg=Register\n");
			registers[1] += 1;
			tempVars[12] = (tempVars[10] >> 4) & 0x3;			//next 2 bits for a register specify the type.  00 is full, 01 is low, 10 mid, 11 high
			tempVars[13] = tempVars[10] & 0xf;				//last 4 bits for a register specify the register number
			//fprintf(stderr, "findArg-RNum %d\n", tempVars[13]);
			if(tempVars[12] == 0)
			{
				//fprintf(stderr, "findArg-RType=0\n");
				return registers[tempVars[13]];			//full 32 bits
			}
			else if(tempVars[12] == 1)
			{
				//fprintf(stderr, "findArg-RType=1\n");
				return registers[tempVars[13]] & 0xFFFF;		//lowest 16 bits
			}
			else if(tempVars[12] == 2)
			{
				//fprintf(stderr, "findArg-RType=2\n");
				return (registers[tempVars[13]] >> 8) & 0xFFFF;	//middle 16 bits
			}
			else if(tempVars[12] == 3)
			{
				//fprintf(stderr, "findArg-RType=3\n");
				return (registers[tempVars[13]] >> 16) & 0xFFFF;	//highest 16 bits
			}
		}
		else if(tempVars[11] == 1)						//type 1: constant integer
		{
			//fprintf(stderr, "findArg=ConstInt\n");
			registers[1] += 1;
			tempVars[12] = tempVars[10] & 0x3F;				//last 6 bits for an integer specify the length of that integer, in bits.  64-bit code liable to explode.
			tempVars32[10] = 0;
			for(tempVars[9] = 0; tempVars[9]<tempVars[12]; tempVars[9]+=8)
			{
				tempVars32[11] = ram[registers[1]];
				tempVars32[10] = tempVars32[10] | (tempVars32[11] << tempVars[9]);		//experimental code for splicing things in.  uses an OR operation.
				registers[1] += 1;
			}
			//fprintf(stderr, "findArg-ITmp %d\n", tempVars32[10]);
			return tempVars32[10];
		}
		else if(tempVars[11] == 2)						//type 2: memory value
		{
			//fprintf(stderr, "findArg=RamVal\n");
			registers[1] += 1;
			tempVars[13] = (tempVars[10] >> 5) & 0x1;			//next 1 bit for a memory addres specifies the flag type
			tempVars[12] = tempVars[10] & 0x1F;				//last 5 bits for a memory address specify the length of the address, in bits.
			fprintf(stderr, "findArgFromRam - length = %d\n", tempVars[12]);
			tempVars32[10] = 0;
			for(tempVars[9] = 0; tempVars[9]<tempVars[12]; tempVars[9]+=8)
			{
				tempVars32[11] = ram[registers[1]];
				tempVars32[10] = tempVars32[10] | (tempVars32[11] << tempVars[9]);		//experimental code for splicing things in.  uses an OR operation.
				registers[1] += 1;
			}
			//fprintf(stderr, "findArg-RITmp %d\n", tempVars32[10]);
			tempVars32[12] = 0;
			if(tempVars32[10] < 0 || tempVars32[10] >= ramLen)
			{
				return 0;
			}
			if(tempVars[13]==0)
			{
				fprintf(stderr, "findArgFromRam - flag 0, look at ram position %d\n", tempVars32[10]);
				tempVars32[12] = ram[tempVars32[10]];
				tempVars32[12] = tempVars32[12] | (ram[tempVars32[10]+1] << 8);
				tempVars32[12] = tempVars32[12] | (ram[tempVars32[10]+2] << 16);
				tempVars32[12] = tempVars32[12] | (ram[tempVars32[10]+3] << 24);
			}
			else
			{
				fprintf(stderr, "findArgFromRam - flag 1, pointer in %d\n", tempVars32[10]);
				tempVars32[14] = 0;
				tempVars32[14] = ram[tempVars32[10]];
				tempVars32[14] = tempVars32[14] | (ram[tempVars32[10]+1] << 8);
				tempVars32[14] = tempVars32[14] | (ram[tempVars32[10]+2] << 16);
				tempVars32[14] = tempVars32[14] | (ram[tempVars32[10]+3] << 24);
				tempVars32[12] = ram[tempVars32[14]];
				tempVars32[12] = tempVars32[12] | (ram[tempVars32[14]+1] << 8);
				tempVars32[12] = tempVars32[12] | (ram[tempVars32[14]+2] << 16);
				tempVars32[12] = tempVars32[12] | (ram[tempVars32[14]+3] << 24);
			}
			return tempVars32[12];
		}
		else if(tempVars[11] == 3)						//type 3: register-pointer
		{
			//fprintf(stderr, "findArg=RegisterP\n");
			registers[1] += 1;
			tempVars[12] = (tempVars[10] >> 4) & 0x3;			//next 2 bits for a register specify the type.  00 is full, 01 is low, 10 mid, 11 high
			tempVars[13] = tempVars[10] & 0xf;				//last 4 bits for a register specify the register number
			//fprintf(stderr, "findArg-RNum %d\n", tempVars[13]);
			tempVars32[14] = 0;
			if(tempVars[12] == 0)
			{
				//fprintf(stderr, "findArg-RType=0\n");
				tempVars32[14] = registers[tempVars[13]];			//full 32 bits
			}
			else if(tempVars[12] == 1)
			{
				//fprintf(stderr, "findArg-RType=1\n");
				tempVars32[14] = registers[tempVars[13]] & 0xFFFF;		//lowest 16 bits
			}
			else if(tempVars[12] == 2)
			{
				//fprintf(stderr, "findArg-RType=2\n");
				tempVars32[14] = (registers[tempVars[13]] >> 8) & 0xFFFF;	//middle 16 bits
			}
			else if(tempVars[12] == 3)
			{
				//fprintf(stderr, "findArg-RType=3\n");
				tempVars32[14] = (registers[tempVars[13]] >> 16) & 0xFFFF;	//highest 16 bits
			}
			if(tempVars32[14] < 0 || tempVars32[14] >= ramLen)
			{
				return 0;
			}
			tempVars32[12] = 0;
			tempVars32[12] = ram[tempVars32[14]];
			tempVars32[12] = tempVars32[12] | (ram[tempVars32[14]+1] << 8);
			tempVars32[12] = tempVars32[12] | (ram[tempVars32[14]+2] << 16);
			tempVars32[12] = tempVars32[12] | (ram[tempVars32[14]+3] << 24);
			return tempVars32[12];
		}
	}
	
	void yapa02SetArg(uint32_t* registers, uint8_t* ram, uint32_t ramLen, uint32_t value, uint8_t vLen, uint32_t* dirtyRam, uint32_t* tempVars32, uint8_t* tempVars)
	{
		tempVars[10] = ram[registers[1]];
		tempVars[11] = (tempVars[10] >> 6) & 0x3; 				//first 2 bits specify argument type
		if(tempVars[11] == 0)
		{
			//fprintf(stderr, "setArg=Register\n");
			registers[1] += 1;
			tempVars[12] = (tempVars[10] >> 4) & 0x3;			//next 2 bits for a register specify the type.  00 is full, 01 is low, 10 mid, 11 high
			tempVars[13] = tempVars[10] & 0xf;				//last 4 bits for a register specify the register number
			//fprintf(stderr, "setArg-RNum %d\n", tempVars[13]);
			if(vLen == 1)
			{
				value = value & 0xFF;
			}
			else if(vLen == 2)
			{
				value = value & 0xFFFF;
			}
			else if(vLen == 3)
			{
				value = value & 0xFFFFFF;
			}
			else
			{
				value = value & 0xFFFFFFFF;
			}
			if(tempVars[12] == 0)
			{
				//fprintf(stderr, "setArg-RType=0\n");
				registers[tempVars[13]]=value;			//full 32 bits
			}
			else if(tempVars[12] == 1)
			{
				//fprintf(stderr, "setArg-RType=1\n");
				tempVars32[14] = registers[tempVars[13]] & 0xFFFF0000;
				registers[tempVars[13]] = tempVars32[14] | (value & 0xFFFF);	//lowest 16 bits
			}
			else if(tempVars[12] == 2)
			{
				//fprintf(stderr, "setArg-RType=2\n");
				tempVars32[14] = registers[tempVars[13]] & 0xFF0000FF;
				registers[tempVars[13]] = tempVars32[14] | ((value & 0xFFFF) << 8);	//middle 16 bits of register are set to lowest 16 of specified value
			}
			else if(tempVars[12] == 3)
			{
				//fprintf(stderr, "setArg-RType=3\n");
				tempVars32[14] = registers[tempVars[13]] & 0x0000FFFF;
				registers[tempVars[13]] = tempVars32[14] | ((value & 0xFFFF) << 16);	//highest 16 bits of register are set to lowest 16 of specified value
			}
		}
		else if(tempVars[11] == 1)						//type 1: constant integer - the destination
		{
			//fprintf(stderr, "setArg=ConstInt\n");
			registers[1] += 1;
			tempVars[12] = tempVars[10] & 0x3F;				//last 6 bits for an integer specify the length of that integer, in bits.  64-bit code liable to explode.
			tempVars32[10] = 0;
			for(tempVars[9] = 0; tempVars[9]<tempVars[12]; tempVars[9]+=8)
			{
				tempVars32[11] = ram[registers[1]];
				tempVars32[10] = tempVars32[10] | (tempVars32[11] << tempVars[9]);		//experimental code for splicing things in.  uses an OR operation.
				registers[1] += 1;
			}
			tempVars[9] = 0;
			if(vLen == 0)						//auto-determine length.
			{
				if(value < 0x100)				//length: 1 byte
				{
					tempVars[9] = 1;
				}
				else if(value < 0x10000)			//length: 2 bytes
				{
					tempVars[9] = 2;
				}
				else if(value < 0x1000000)			//length: 3 bytes
				{
					tempVars[9] = 3;
				}
				else if(value < 0x100000000)			//length: 4 bytes
				{
					tempVars[9] = 4;
				}
			}
			else
			{
				tempVars[9] = vLen;
			}
			//fprintf(stderr, "setArg-ITmp %d\n", tempVars32[10]);
			//fprintf(stderr, "setArg-IVal %d\n", value);
			if(tempVars32[10] < 0 || tempVars32[10] > ramLen)
			{
				return;
			}
			ram[tempVars32[10]] = value;
			tempVars32[15] = tempVars32[10];
			inDirtyRam(dirtyRam, tempVars32);
			if(tempVars32[9]==-1)
				dirtyRam[dirtyRam[0]+1] = tempVars32[10];
			if(tempVars[9] > 1)						//length: 2 bytes
			{
				ram[tempVars32[10]+1] = value >> 8;
				tempVars32[15] = tempVars32[10]+1;
				inDirtyRam(dirtyRam, tempVars32);
				if(tempVars32[9]==-1)
					dirtyRam[dirtyRam[0]+2] = tempVars32[10]+1;
			}
			if(tempVars[9] > 2)						//length: 2 bytes
			{
				ram[tempVars32[10]+2] = value >> 16;
				tempVars32[15] = tempVars32[10]+2;
				inDirtyRam(dirtyRam, tempVars32);
				if(tempVars32[9]==-1)
					dirtyRam[dirtyRam[0]+3] = tempVars32[10]+2;
			}
			if(tempVars[9] > 3)						//length: 2 bytes
			{
				ram[tempVars32[10]+3] = value >> 24;
				tempVars32[15] = tempVars32[10]+3;
				inDirtyRam(dirtyRam, tempVars32);
				if(tempVars32[9]==-1)
					dirtyRam[dirtyRam[0]+4] = tempVars32[10]+3;
			}
			dirtyRam[0] = dirtyRam[0]+tempVars[9];
		}
		else if(tempVars[11] == 2)						//type 2: memory value
		{
			//fprintf(stderr, "setArg=RamVal\n");
			registers[1] += 1;
			tempVars[13] = (tempVars[10] >> 5) & 0x1;			//next 1 bit for a memory addres specifies the flag type
			tempVars[12] = tempVars[10] & 0x1F;				//last 5 bits for a memory address specify the length of the address, in bits.
			tempVars32[10] = 0;
			for(tempVars[9] = 0; tempVars[9]<tempVars[12]; tempVars[9]+=8)
			{
				tempVars32[11] = ram[registers[1]];
				tempVars32[10] = tempVars32[10] | (tempVars32[11] << tempVars[9]);		//experimental code for splicing things in.  uses an OR operation.
				registers[1] += 1;
			}
			//fprintf(stderr, "setArg-RITmp %d\n", tempVars32[10]);
			tempVars32[13] = 0;
			if(tempVars32[10] < 0 || tempVars32[10] > ramLen)
			{
				return;
			}
			if(tempVars[12]==0)
			{
				tempVars32[13] = ram[tempVars32[10]];
				tempVars32[13] = tempVars32[13] | (ram[tempVars32[10]+1] << 8);
				tempVars32[13] = tempVars32[13] | (ram[tempVars32[10]+2] << 16);
				tempVars32[13] = tempVars32[13] | (ram[tempVars32[10]+3] << 24);
			}
			else
			{
				tempVars32[14] = 0;
				tempVars32[14] = ram[tempVars32[10]];
				tempVars32[14] = tempVars32[14] | (ram[tempVars32[10]+1] << 8);
				tempVars32[14] = tempVars32[14] | (ram[tempVars32[10]+2] << 16);
				tempVars32[14] = tempVars32[14] | (ram[tempVars32[10]+3] << 24);
				tempVars32[13] = ram[tempVars32[14]];
				tempVars32[13] = tempVars32[13] | (ram[tempVars32[14]+1] << 8);
				tempVars32[13] = tempVars32[13] | (ram[tempVars32[14]+2] << 16);
				tempVars32[13] = tempVars32[13] | (ram[tempVars32[14]+3] << 24);
			}
			tempVars[9] = 0;
			if(vLen == 0)						//auto-determine length.
			{
				if(value < 0x100)				//length: 1 byte
				{
					tempVars[9] = 1;
				}
				else if(value < 0x10000)			//length: 2 bytes
				{
					tempVars[9] = 2;
				}
				else if(value < 0x1000000)			//length: 3 bytes
				{
					tempVars[9] = 3;
				}
				else if(value < 0x100000000)			//length: 4 bytes
				{
					tempVars[9] = 4;
				}
			}
			else
			{
				tempVars[9] = vLen;
			}
			//fprintf(stderr, "setArg-RIVal %d\n", value);
			ram[tempVars32[13]] = value;
			tempVars32[15] = tempVars32[13];
			inDirtyRam(dirtyRam, tempVars32);
			if(tempVars32[9]==-1)
				dirtyRam[dirtyRam[0]+1] = tempVars32[13];
			if(tempVars[9] > 1)						//length: 2 bytes
			{
				ram[tempVars32[13]+1] = value >> 8;
				tempVars32[15] = tempVars32[13]+1;
				inDirtyRam(dirtyRam, tempVars32);
				if(tempVars32[9]==-1)
					dirtyRam[dirtyRam[0]+2] = tempVars32[13]+1;
			}
			if(tempVars[9] > 2)						//length: 2 bytes
			{
				ram[tempVars32[13]+2] = value >> 16;
				tempVars32[15] = tempVars32[13]+2;
				inDirtyRam(dirtyRam, tempVars32);
				if(tempVars32[9]==-1)
					dirtyRam[dirtyRam[0]+3] = tempVars32[13]+2;
			}
			if(tempVars[9] > 3)						//length: 2 bytes
			{
				ram[tempVars32[13]+3] = value >> 24;
				tempVars32[15] = tempVars32[13]+3;
				inDirtyRam(dirtyRam, tempVars32);
				if(tempVars32[9]==-1)
					dirtyRam[dirtyRam[0]+4] = tempVars32[13]+3;
			}
			dirtyRam[0] = dirtyRam[0]+tempVars[9];
		}
		else if(tempVars[11] == 3)						//type 3: register-pointer
		{
			//fprintf(stderr, "setArg=RegisterP\n");
			registers[1] += 1;
			tempVars[12] = (tempVars[10] >> 4) & 0x3;			//next 2 bits for a register specify the type.  00 is full, 01 is low, 10 mid, 11 high
			tempVars[13] = tempVars[10] & 0xf;				//last 4 bits for a register specify the register number
			//fprintf(stderr, "setArg-RNum %d\n", tempVars[13]);
			tempVars32[14] = 0;
			if(tempVars[12] == 0)
			{
				//fprintf(stderr, "setArg-RType=0\n");
				tempVars32[14] = registers[tempVars[13]];			//full 32 bits
			}
			else if(tempVars[12] == 1)
			{
				//fprintf(stderr, "setArg-RType=1\n");
				tempVars32[14] = registers[tempVars[13]] & 0xFFFF;		//lowest 16 bits
			}
			else if(tempVars[12] == 2)
			{
				//fprintf(stderr, "setArg-RType=2\n");
				tempVars32[14] = (registers[tempVars[13]] >> 8) & 0xFFFF;	//middle 16 bits
			}
			else if(tempVars[12] == 3)
			{
				//fprintf(stderr, "setArg-RType=3\n");
				tempVars32[14] = (registers[tempVars[13]] >> 16) & 0xFFFF;	//highest 16 bits
			}
			tempVars[9] = 0;
			if(vLen == 0)						//auto-determine length.
			{
				if(value < 0x100)				//length: 1 byte
				{
					tempVars[9] = 1;
				}
				else if(value < 0x10000)			//length: 2 bytes
				{
					tempVars[9] = 2;
				}
				else if(value < 0x1000000)			//length: 3 bytes
				{
					tempVars[9] = 3;
				}
				else if(value < 0x100000000)			//length: 4 bytes
				{
					tempVars[9] = 4;
				}
			}
			else
			{
				tempVars[9] = vLen;
			}
			//fprintf(stderr, "setArg-RIVal %d\n", value);
			if(tempVars32[14] < 0 || tempVars32[14] > ramLen)
			{
				return;
			}
			ram[tempVars32[14]] = value;
			tempVars32[15] = tempVars32[14];
			inDirtyRam(dirtyRam, tempVars32);
			if(tempVars32[9]==-1)
				dirtyRam[dirtyRam[0]+1] = tempVars32[14];
			if(tempVars[9] > 1)						//length: 2 bytes
			{
				ram[tempVars32[14]+1] = value >> 8;
				tempVars32[15] = tempVars32[14]+1;
				inDirtyRam(dirtyRam, tempVars32);
				if(tempVars32[9]==-1)
					dirtyRam[dirtyRam[0]+2] = tempVars32[14]+1;
			}
			if(tempVars[9] > 2)						//length: 2 bytes
			{
				ram[tempVars32[14]+2] = value >> 16;
				tempVars32[15] = tempVars32[14]+2;
				inDirtyRam(dirtyRam, tempVars32);
				if(tempVars32[9]==-1)
					dirtyRam[dirtyRam[0]+3] = tempVars32[14]+2;
			}
			if(tempVars[9] > 3)						//length: 2 bytes
			{
				ram[tempVars32[14]+3] = value >> 24;
				tempVars32[15] = tempVars32[14]+3;
				inDirtyRam(dirtyRam, tempVars32);
				if(tempVars32[9]==-1)
					dirtyRam[dirtyRam[0]+4] = tempVars32[14]+3;
			}
			dirtyRam[0] = dirtyRam[0]+tempVars[9];
		}
	}
	
	void yapa02Compute(int numTicks, uint32_t* halted, uint32_t* registers, uint8_t* ram, uint32_t ramLen, uint8_t* perData, uint32_t numPers, uint32_t* perLens, uint32_t* dirtyRam, uint32_t* tempVars32, uint8_t* tempVars, uint8_t interruptID, uint32_t interruptData, uint32_t* interruptList)
	{
		tempVars[8] = 0;
		if(registers[1] < 0 || registers[1] >= ramLen)
		{
			halted[0] = -1;
		}
		else
		{
			if(interruptID!=0 && interruptList[interruptID]!=0 && tempVars[8] == 0)
			{
				registers[4] = registers[1];
				registers[1] = interruptList[interruptID];
				registers[3] = interruptData;
				tempVars[8] = 1;
				halted[0] = 0;
			}
		}
		for(int i = 0; i<numTicks; i++)
		{
			for(int j = 0; j<16; j++)
			{
				tempVars32[j] = 0;
				tempVars[j] = 0;
			}
			if(halted[0] == -1)					//computer is permanently halted
			{
				break;
			}
			else if(halted[0] > 0)					//computer is indefinitely halted
			{
				halted[0] = halted[0]-1;
			}
			else							//computer is not halted
			{
				registers[0] = registers[1];			//set PC = IA
				//fprintf(stderr, "PC=IA\n");
				tempVars[0] = ram[registers[1]];		//the opcode byte
				//fprintf(stderr, "found opcode byte: %d\n", tempVars[0]);
				tempVars[1] = (tempVars[0] >> 4) & 0xf;		//gets the actual opcode from that byte
				//fprintf(stderr, "opcode picked: %d\n", opcode);
				tempVars[2] = tempVars[0] & 0xf;			//gets the flag from that byte
				//fprintf(stderr, "flag picked: %d\n", flag);
				registers[1] = registers[1]+1;			//increment the instruction address
				//fprintf(stderr, "incremented instruction address\n");
				if(tempVars[1] == 0)					//NIL: no arguments
				{
					//fprintf(stderr, "NIL\n");
					//NIL, it doesn't do anything.
				}
				else if(tempVars[1] == 1)				//HLT: potentially 1 argument
				{
					//fprintf(stderr, "HLT\n");
					if(tempVars[2] == 0)				//flag 0: permanent halt
					{
						//fprintf(stderr, "permanent\n");
						halted[0] = -1;
					}
					else					//flag 1: temporary halt
					{
						//fprintf(stderr, "timed\n");
						halted[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
					}
				}
				else if(tempVars[1] == 2)				//MOV: two arguments
				{
					//fprintf(stderr, "MOV\n");
					tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
					yapa02SetArg(registers, ram, ramLen, tempVars32[0], tempVars[2], dirtyRam, tempVars32, tempVars);
				}
				else if(tempVars[1] == 3)				//JMP: flag used, one argument
				{
					//fprintf(stderr, "JMP\n");
					tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
					if(tempVars[2] >= 3)				//Is JSR.
					{
						tempVars[2] -= 3;
						registers[2] = registers[1];
					}
					if(tempVars[2] == 0)				//flag 0: relative jump (PC+=X)
					{
						//fprintf(stderr, "relative\n");
						registers[1] = registers[0] = registers[0]+tempVars32[0];
					}
					else if(tempVars[2] == 1)			//flag 1: direct jump (PC=X)
					{
						//fprintf(stderr, "direct\n");
						registers[1] = registers[0] = tempVars32[0];
					}
					else if(tempVars[2]== 2)			//flag 2: backwards relative jump (PC-=X)
					{
						//fprintf(stderr, "back-relative\n");
						registers[1] = registers[0] = registers[0]-tempVars32[0];
					}
				}
				else if(tempVars[1] == 4)				//OPR: arguments vary by flag
				{
					//fprintf(stderr, "OPR\n");
					if(tempVars[2] == 0)				//ADD: source, value, destination
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]+tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
					}
					else if(tempVars[2] == 1)			//SUB: source, value, destination
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]-tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
					}
					else if(tempVars[2] == 2)			//MUL: source, value, destination
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]*tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
					}
					else if(tempVars[2] == 3)			//DIV: source, value, destination
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]/tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
					}
					else if(tempVars[2] == 4)			//DVR: source, value, destination, remainder-destination
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]/tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]%tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
					}
					else if(tempVars[2] == 5)			//MOD: source, value, destination
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]%tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
					}
					else if(tempVars[2] == 6)			//AND: source, mask, destination
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]&tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
					}
					else if(tempVars[2] == 7)			//BOR: source, mask, destination
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]|tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
					}
					else if(tempVars[2] == 8)			//XOR: source, mask, destination
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]^tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
					}
					else if(tempVars[2] == 9)            //SHL: source, # of bits, destination
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]<<tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
					}
					else if(tempVars[2] == 10)            //SHR: source, # of bits, destination
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);
						yapa02SetArg(registers, ram, ramLen, tempVars32[0]>>tempVars32[1], 4, dirtyRam, tempVars32, tempVars);
					}
				}
				else if(tempVars[1] == 5)				//BRN: flag used, two arguments
				{
					//fprintf(stderr, "BRN\n");
					tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);	//val1
					tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);	//val2
					tempVars[3] = 9;								//doJump
					if(tempVars[2]%4 == 0)				//BEQ: branch on equal
					{
						//fprintf(stderr, "BEQ\n");
						if(tempVars32[0]==tempVars32[1])
						{
							//fprintf(stderr, "BEQ SUCCESS\n");
							tempVars[3] = tempVars[2]/4;
						}
					}
					else if(tempVars[2]%4 == 1)			//BNE: branch on not-equal
					{
						//fprintf(stderr, "BNE\n");
						if(tempVars32[0]!=tempVars32[1])
						{
							//fprintf(stderr, "BNE SUCCESS\n");
							tempVars[3] = tempVars[2]/4;
						}
					}
					else if(tempVars[2]%4 == 2)			//BGT: branch on greater-than
					{
						//fprintf(stderr, "BGT\n");
						if(tempVars32[0]>=tempVars32[1])
						{
							//fprintf(stderr, "BGT SUCCESS\n");
							tempVars[3] = tempVars[2]/4;
						}
					}
					else if(tempVars[2]%4 == 3)			//BLT: branch on less-than
					{
						//fprintf(stderr, "BLT\n");
						if(tempVars32[0]<tempVars32[1])
						{
							//fprintf(stderr, "BLT SUCCESS\n");
							tempVars[3] = tempVars[2]/4;
						}
					}
					//fprintf(stderr, "FLAG: %d\n", tempVars[2]);
					//fprintf(stderr, "FLAG MOD: %d\n", tempVars[2]%4);
					//fprintf(stderr, "JUMP TYPE: %d\n", tempVars[3]);
					//fprintf(stderr, "REG 1: %d\n", registers[1]);
					if(tempVars[3] == 0)				//JMP 0 equivalent
					{
						tempVars32[2] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);	//destination
						registers[1] = registers[0] = registers[0]+tempVars32[2];
					}
					else if(tempVars[3] == 1)			//JMP 1 equivalent
					{
						tempVars32[2] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);	//destination
						registers[1] = registers[0] = tempVars32[2];
					}
					else if(tempVars[3] == 2)			//JMP 2 equivalent
					{
						tempVars32[2] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);	//destination
						registers[1] = registers[0] = registers[0]-tempVars32[2];
					}
					else if(tempVars[3] == 9)
					{
						tempVars32[2] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);	//destination
						//fprintf(stderr, "DO NOT JUMP DO NOT JUMP DO NOT JUMP\n");
					}
				}
				else if(tempVars[1] == 6)				//PGM: arguments vary by flag
				{
					//fprintf(stderr, "PGM\n");
				}
				else if(tempVars[1] == 7)				//INT: arguments vary by flag
				{
					//fprintf(stderr, "INT\n");
					if(tempVars[2] == 0)
					{
						tempVars32[0] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);	//interrupt ID
						tempVars32[0] = tempVars32[0]%256;
						tempVars32[1] = yapa02FindArg(registers, ram, ramLen, tempVars32, tempVars);	//destination address
						interruptList[tempVars32[0]] = tempVars32[1];
					}
				}
			}
		}
	}
	
	void yapa02GenMon(uint8_t* ram, uint8_t* monRam, uint32_t monLen, uint32_t* tempI)
	{
		for(tempI[0] = 0; tempI[0]<monLen; tempI[0]++)
		{
			monRam[tempI[0]] = ram[tempI[0]];
		}
	}
}
