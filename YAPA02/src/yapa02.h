#ifndef LIBYAPA02_H
#define LIBYAPA02_H

#include <stdint.h>
//registers[6:15] are R0:9, respectively, the main user registers.  [0]=PC, [1]=IA, [2]=RT, [3]=ID, [4]=IR
extern "C"
{
	void yapa02Compute(int numTicks, uint32_t* halted, uint32_t* registers, uint8_t* ram, uint32_t ramLen, uint8_t* perData, uint32_t numPers, uint32_t* perLens, uint32_t* dirtyRam, uint32_t* tempVars32, uint8_t* tempVars, uint8_t interruptID, uint32_t interruptData, uint32_t* interruptList);
	uint32_t yapa02FindArg(uint32_t* registers, uint8_t* ram, uint32_t ramLen, uint32_t* tempVars32, uint8_t* tempVars);
	void yapa02SetArg(uint32_t* registers, uint8_t* ram, uint32_t ramLen, uint32_t value, uint8_t vLen, uint32_t* dirtyRam, uint32_t* tempVars32, uint8_t* tempVars);
	void yapa02GenMon(uint8_t* ram, uint8_t* monRam, uint32_t monLen, uint32_t* tempI); //used to generate a monitor RAM pointer.  at some point.  for now, just copies ram data.
}
#endif //LIBRISC06_H
