Content-Type: text/x-zim-wiki
Wiki-Format: zim 0.4
Creation-Date: 2018-08-10T22:14:45-04:00

====== CPU info ======
All RAM (and ROM, and drives) use 32-bit memory addresses.

ALL processors have 16 addresses of 32-bit Register memory.  (64bytes, 4/register)  Register memory is where most important things happen.
Register 0 may one day contain the program counter.  Not sure on it yet.
GETDATA always spits its results out to Register 0x1.
IFJMP compares the value of Register 0x2 to 0x3.
Registers 0x4 to 0xf are all unused and free to work with for your program.

==== IMPORTANT NOTES ====
* The TC-06 architecture (and assembler for it) were written by a **C# PROGRAMMER**.  Think like one while working for it - this ISN'T like IRL assembly languages (such as the 6502's), and it's important to keep that in mind while you play.
* SET works like an array, treating registers like a length-4 array of Bytes.  SET 0 0 01000000 sets registers[0].bytes[0] = 0b01000000.  It DOES accept integer arguments (though they're still WIP and not useful in every situation), but was meant to manually set bits.
* For a more visual representation: 00000000111111112222222233333333
* All integers are handled as standard 32-bit C# integers.  That includes the contents of RAM/registers/disks - they're literally arrays of type **int**.
* Bits & bytes start at the right.  A byte with the value 1 to us would be 00000001.  3 would be 00000011.  And so on.  A register whose contents are the integer 255 would look like 00000000000000000000000011111111 in raw bit form.
* Also, because I think it MIGHT be confusing: //SETDATA// **IS NOT** //SET//.  They're both useful opcodes, and they often go together (say, for drawing a pixel from a register value), but SETDATA runs a function on a specified dataport, while SET sets the contents of a register.

===== OP-CODES =====
These're the op-codes you can actually put into the Assembler.  Beware that formatting errors & argument misunderstandings could potentially cause the underlying C# simulation to error out - if your computer seems to freeze without actually being halted, and remains on the same address in the Debugger window?  There's something wrong with that command.  It may be of help to check the game log files (~/.config/unity3d/The 006 Cooperative/SenbirGame/player.log in Linux, C:\Users\YourName\AppData\Local\The 006 Cooperative\SenbirGame\player.log in Windows (probably), and as-of-yet unknown in Macs) to see what the actual error is.

=== OP-CODE: DATAC (N/A) <data=32b> ===
* Used solely for the compiler, this lets you put in a raw bit value.

=== OP-CODE: NILLIST (N/A) <numLines> ===
* Used solely for the compiler, this lets you add in as many blank memory addresses as you want.
* Useful if you want to pad something out to the end of memory.

=== OP-CODE: NIL (0x0000) ===
* Does nothing.  Equivalent to DATAC 0x00000000000000000000000000000000.
* Very useful for memory locations that you plan to use as variables at some point.

=== OP-CODE: HLT (0x0001) [ticks = 28b] ===
* Halt the processor.  If ticks==0, it halts until reboot.
* If ticks>0, it waits (i.e, 0x0001000...0001)...
* ...the number of clock cycles specified in ticks.

=== OP-CODE: MOVI (0x0010) <register = 4b> <address = 24b> ===
* Move the data at the specified address into the specified register.

=== OP-CODE: MOVO (0x0011) <register = 4b> <address = 24b> ===
* Move the data from the specified register out into the specified address.

=== OP-CODE: JMP (0x0100) <flag = 2b> <address = 24b> <null data = 2b> ===
* Jump to the memory address <address>es ahead of us if flag == 0.
* Jump to the specified memory address if flag == 1.
* Jump backwards <address>es if flag == 2.
* Jump to (maxADDR)-<address> if flag == 3.  (maxADDR) is the maximum possible memory address on this device - e.g, a 4kb device would have (maxADDR)=1023.
* JMP will immediately execute whatever memory address it jumps to, effectively making it take no clock cycles in-and-of-itself.
* This means it WILL immediately execute other JMP operations.  Which can then immediately execute other JMP operations.
* If a recursive loop of jumps occurrs, your computer will crash.  I.E, RAM[0]=JMP 0 1, RAM[1]=JMP 2 1
* If you see the halt light come on and steam floats out - this is what killed it.  Turn it off, check your code for recursion, and fix it.
* JMP 0 1 jumps 1 address forward, in much the same way normal functions will (i.e, MOVI and MOVO will also jump 1 address forwards after execution is over)
* JMP 1 0 jumps to ABSOLUTE memory address zero.  This is likely to be the more familiar one for Assembly vets.  It's 0-indexed, like an array - jumping to the end of Default RAM would be JMP 1 31.
* JMP 2 1 jumps 1 address //backwards//, a reverse of flag 0.
* JMP 3 1 jumps to the next-to-last memory address, an inverse of flag 1.

=== OP-CODE: SETDATA (0x0101) <target port = 4b> <flag = 2b> <data for flag = 22b> ===
* Runs the universal [[SetData]] function on a target DataPort.
* If flag == 0, the data being passed in is assumed to be a 22-bit CONSTANT, shifted left by 11 bits to ensure it...y'know, works.  Literally type in a string of 22 1s and 0s for this.
* If flag == 1, the data being passed in is assumed to be a 21-bit RELATIVE ADDRESS preceded by a 1-bit FLAG for if it's relative //behind// this operation, or ahead of it.  It'll read from that address and pass it on to the actual DataPort.
* If flag == 2, the data being passed in is assumed to be a 21-bit ABSOLUTE ADDRESS preceded by a 1-bit FLAG for if it's going forwards from address 0, or backwards from address (maxADDR) - see above.  It'll read from that address and pass it on to the actual DataPort.
* If flag == 3, the data being passed in is assumed to be a 4-bit REGISTER  ADDRESS with 18 bits of NULL DATA after it.    It'll read from that address and pass it on to the actual DataPort.
* NOTE: You can perform an EXTENDED SET-DATA OPERATION with an extra 1 flag followed by a SECOND REGISTER ADDRESS.  This only works with flag 3.
* The assembler will automatically do this for you, i.e, SETDATA 0 3 0 1 will run the extended setdata function on the monitor, passing the contents of Register 0 and 1 in for its arguments.

=== OP-CODE: GETDATA (0x0110) <target port = 4b> <flag = 2b> <data for flag = 22b> ===
* Runs the universal [[GetData]] function on a target DataPort, outputting the result to REGISTER 1 (0x1, or 0001)
* Flag & data work the same as with SetData.

=== OP-CODE: SET (0x01111) <register = 4b> <startPos = 2b> <data = 8b> ===
* Put a byte into one of four bytes stored in the specified register.

=== OP-CODE: IFJMP (0x1000) <flag = 2b> <address = 24b> <operation = 2b> ===
* Compares the value of Register 0x2 to 0x3, jumping to the specified address if the operation returns true., otherwise it counts forward normally.  See also, JMP documentation for how flag & address are used.
* OPERATION 0x0: Equals - if R0x2==R0x3, it's true.
* OPERATION 0x1: Not Equals - if R0x2!=R0x3, it's true.
* OPERATION 0x2: Greater Than - if R0x2>R0x3, it's true.
* OPERATION 0x3: Less Than - if R0x2<R0x3, it's true.
* The same execute-on-jump thing happens here, too, with the same caveat to avoid recursion.

=== OP-CODE: PMOV (0x1001) <registry source = 4b> <registry target = 4b> <start = 5b> <end = 5b> <shift = 5b> <shiftBack = 1b> ===
* Copy & paste a specific selection of bits from the SOURCE (see start & end) to the TARGET.
* They retain their position (i.e, start=3 end=8 will paste bits 3 through 8 from the source over bits 3 through 8 in the target), BUT...
* ...the Shift option lets you shift the destination up to 31 units right, wrapping around as need-be.  Bits 3 through 8 from the source with an offset of 2 paste into bits 5 through 10 in the destination.  shiftBack being one will make it shift left (i,e, the offset two example would give bits 1 through 6)
* Please note that PMOV is a WORK IN PROGRESS function, still, and may not work entirely as predicted.

=== OP-CODE: MATH (0x1010) <registry source = 4b> <registry target = 4b> <operation = 4b> [extra data = 16b] ===
* Apply a math operation of some kind.
* Operation 0x0: Add the value of SOURCE to TARGET.
* Operation 0x1: Subtract the value of SOURCE from TARGET.
* Operation 0x2: Multiply TARGET by SOURCE.
* Operation 0x3: Divide TARGET by SOURCE.  Follows C# int/int model.
* Operation 0x4: Modulo TARGET by SOURCE.  Follows C# int%int model.
* Operation 0x5: Set TARGET to SOURCE.
* Operation 0x6: Set TARGET to Random.Range(0, SOURCE)
* Further operations: TBD

=== OP-CODE: UTL (0x1011) <sub-code = 4b> <arguments = 24b> ===
* Gives 16 new sub-op-codes, primarily utility functions, like enabling an offset to Absolute operations (e.g, MOVI 1 0  becomes MOVI 1 0+offset)
* If a sub-code is given a title, then you can refer to it by name directly in the Assembler - i.e, UTL 0 0 has the same result as OFST 0 in the compiled bytecode.
* Operation 0x0 - OFST <24-bit address>: Sets an OFFSET (from address 0) for all ABSOLUTE ADDRESSING.  Affects MOVI, MOVO, SETDATA flag 2, and GETDATA flag 2.  Uses the full 24 bits given to it.
	* OFST 5 sets a 5-address offset to all absolute address references - //save for// those in IFJMP & JMP with Flag 1 or Flag 3.
	* This means OFST 5 followed by MOVI 1 0 will result in MOVI moving the contents of address **5** (0 + 5) into register 1.
	* But JMP 1 0 will still jump to & execute the contents of RAM address 0.  Since flags 0 and 2 provide relative addressing, it's not necessary.
	* OFST 0 will also reset it so things work as expected.
* Operation 0x1 - PCSR <4-bit sub-sub code> <20-bit argument>: Subcodes upon subcodes!  This lets you interact with the processor somewhat.
	* PCSR 0 <arguments ignored>: Get CURRENT COUNTER POSITION.  Similar to a GETDATA op, but for processor info instead.  Returns the current counter position in Register 1.
	* PCSR 1 <arguments ignored>: Get CURRENT OFFSET.  Same as PCSR 0, but for the offset instead.
	* PCSR 2 <1-bit flag>: Temporarily ENABLE or DISABLE the offset.
* Operation 0x2 - TIMR <2-bit OFF, START, PAUSE, GET value> <22-bit TIMER ID>: Times the number of cycles run by the processor.
	* TIMR OFF <id> (argument 0x0): The default state.  Sets the # of cycles to 0, and won't increment them.
	* TIMR START <id> (argument 0x1): Sets the # of cycles to 0, and starts incrementing it with each clock cycle run.
	* TIMR PAUSE <id> (arguiment 0x2): Pauses incrementation, but leaves the # of cycles alone.  If run while paused, resumes cycling.
	* TIMR GET <id> (argument 0x3): Sets the contents of Register 1 to be the number of clock cycles run since the last call of TIMR SET.
	* The ID is useful if you're working in, say, a multi-tasked environment, and you want your program to time itself so it won't run over a requested number of cycles.
* Operation 0x3 - MOVR <1-bit flag> <4-bit source-addr register> <4-bit dest-addr register> <remaining bits ignored>: Like MOVI and MOVO, but lets you MOV directly from one RAM location to another.
	* MOVR 0 <x> <y>: When the flag is 0, it uses ABSOLUTE ADDRESSING, no matter what.  If registers[x]==4, then the source address will always be 4.
	* MOVR 1 <x> <y>: When the flag is 1, it will use RELATIVE ADDRESSING only if OFST is active.  In that case, the source address would be registers[x]+OFST; say an offset of 5, and [x]=4, that gives 4+5 for a source address of 9.
* Operation 0x4 - INTR <2-bit operation> <8-bit interrupt class> [4-bit register specifying memory address for interrupt, required by certain operations]
	* This op-code enables INTERRUPTS.  The list of interrupts is currently **WORK IN PROGRESS** and therefor prone to change.
	* Interrupt data will go in REGISTER 1.  Current address (for jumping back to the executing code) will go in REGISTER 0.
	* INTR 0 <interrupt> <register>: Enables [[RunInterrupts]] for the specified class.  See the link for info on what those interrupts are.
	* INTR 1 <interrupt>: Disables interrupts on the specified class.
	* INTR 2 <interrupt> <compOp [2b]> <compRegS [4b]> <const-flag [1b]> <compRegT [4b]> <destination register>: Sets a custom comparison interrupt, ostensibly IFJMP but able to run at any time.
		* The constant flag toggles if the value should be stored (e.g, the "interrupt on [value]" remains constant based on the starting value of the register), or if it should store the register (e.g, the value can change at runtime)
		* This can potentially override an already-registered interrupt.  Register the original again, or disable, to be rid of it - e.g, if you overwrote slot 1 (keyboard read failed), INTR 1 1 would disable it outright, or INTR 0 1 <id> would restore a normal keyboard-read-failure interrupt.
* Operation 0x5 - TBD
* Further operations: TBD

=== FURTHER OP-CODES: TBD ===
_{run jmp 0 0 for a special surprise}
