﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortMouse : DataPort
{
	public Vector2 accumulated;
	public bool[] buttonState;
	public int sensitivity;

	public override int GetData(int data) //32B address
	{
		int r = 0;
		int flag = (data >> 30) & 0x3;
		if(flag == 0)
		{
			r = (int)(accumulated.x * sensitivity);
			accumulated.x = 0;
		}
		else if(flag == 1)
		{
			r = (int)(accumulated.y * sensitivity);
			accumulated.y = 0;
		}
		else if(flag == 2)
		{
			int reqKey = (data >> 28) & 0xF;
			r = buttonState[reqKey] ? 1 : 0;
		}
		return r; //DO NOTHING
	}

	public override void SetData(int data) //24B DATA - SET POINTER
	{
		for(int i = 0; i < buttonState.Length; i++)
		{
			buttonState[i] = false;
		}
		accumulated = Vector2.zero;
	}

	public override void ExtendedSetData(int data1, int data2) //32B DATA - EXPECTS A FULL 32 BITS OF INPUT
	{
		SetData(data1);
	}

	public override void PowerOff()
	{
		for(int i = 0; i < buttonState.Length; i++)
		{
			buttonState[i] = false;
		}
		accumulated = Vector2.zero;
	}
}