﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class DataPort : MonoBehaviour
{
	public abstract void SetData(int data);
	public abstract void ExtendedSetData(int data1, int data2);
	public abstract int GetData(int data);
	public abstract void PowerOff();
}