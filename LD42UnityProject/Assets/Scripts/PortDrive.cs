﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortDrive : DataPort
{
	// The actual storage of the drive, similar to RAM.
	public int[] driveStorage;
	public int curPointer;
	public int driveID;

	void Start()
	{
		//if(GameManager.instance != null)
		{
			driveStorage = GameManager.driveStorage;
		}
	}

	public override int GetData(int data) //32B address
	{
		//Debug.Log("GetData:"+data);
		int address = data;
		return driveStorage[address];
	}

	public override void SetData(int data) //24B DATA - SET POINTER
	{
		int address = data;
		if(curPointer == -1)
		{
			curPointer = address;
		}
		else
		{
			driveStorage[curPointer] = address;
			curPointer = -1;
		}
	}

	public override void ExtendedSetData(int data1, int data2) //32B DATA - EXPECTS A FULL 32 BITS OF INPUT
	{
		driveStorage[data1] = data2;
	}

	public override void PowerOff()
	{
		//DO NOTHING
	}
}