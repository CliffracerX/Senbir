﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Computer : MonoBehaviour
{
	/// <summary>
	/// This system's RAM.
	/// </summary>
	public int[] ram;
	/// <summary>
	/// Read-Only memory that gets copied into RAM at runtime.
	/// </summary>
	public int[] rom;
	/// <summary>
	/// This device's registers.
	/// </summary>
	public int[] registers = new int[16];
	/// <summary>
	/// Is the system powered on?
	/// </summary>
	public bool powerOn;
	/// <summary>
	/// Is the system halted?
	/// </summary>
	public bool halted = false;
	/// <summary>
	/// In Debug Mode, the clock only ticks when the player asks it to.
	/// </summary>
	public bool debugMode = false;
	/// <summary>
	/// The program counter used to, well, run programs.  Represents a memory address in RAM somewhere.
	/// </summary>
	public int counter;
	/// <summary>
	/// Used for HLT <ticks> to temporarily halt the processor.
	/// </summary>
	public int haltTime;
	/// <summary>
	/// What is this processor's clock speed in Hertz?
	/// </summary>
	public int cyclesPerSec;
	/// <summary>
	/// A list of DataPorts connected to this system.
	/// </summary>
	public DataPort[] ports;
	/// <summary>
	/// Do we automatically power the computer?
	/// </summary>
	public bool powerAtStart;
	/// <summary>
	/// Addresses run this clock cycle.
	/// Used to avoid a recursive loop/stack overflow in JMP and IFJMP.
	/// </summary>
	public List<int> runAddresses;
	/// <summary>
	/// Global offset used with all absolute addresses (e.g, MOVI/MOVO/SETDATA/GETDATA)
	/// Set with UTL operation 0 - OFST.
	/// </summary>
	public int globalOffset;
	/// <summary>
	/// The number of times the cycle function should run per tick from InvokeRepeating.
	/// </summary>
	public int intC;

	public Light powerLight,haltLight,dbgLight;

	void Start()
	{
		if(powerAtStart)
		{
			TogglePower();
		}
		ram = new int[GameManager.ram];
		cyclesPerSec = GameManager.cyclesPerSec;
		intC = cyclesPerSec / 125;
		if(intC < 1)
		{
			intC = 1;
		}
		InvokeRepeating("DoTick", 0.0f, 1.0f/(cyclesPerSec/(intC)));
	}

	public void TogglePower()
	{
		if(powerOn)
		{
			for(int i = 0; i<ram.Length; i++)
			{
				ram[i] = 0;
			}
			for(int i = 0; i<registers.Length; i++)
			{
				registers[i] = 0;
			}
			for(int i = 0; i<ports.Length; i++)
			{
				ports[i].PowerOff();
			}
			powerOn = false;
			halted = false;
			counter = 0;
			haltTime = 0;
			if(powerLight != null)
				powerLight.enabled = false;
			globalOffset = 0;
		}
		else
		{
			for(int i = 0; i<rom.Length; i++)
			{
				ram[i] = rom[i];
			}
			for(int i = 0; i<registers.Length; i++)
			{
				registers[i] = 0;
			}
			for(int i = 0; i<ports.Length; i++)
			{
				ports[i].PowerOff();
			}
			powerOn = true;
			halted = false;
			counter = 0;
			haltTime = 0;
			if(powerLight != null)
				powerLight.enabled = true;
			globalOffset = 0;
		}
	}

	void DoTick()
	{
		if(haltLight != null)
		{
			haltLight.enabled = (halted || haltTime > 0) && powerOn;
			if(powerLight != null)
			{
				powerLight.enabled = powerOn && !haltLight.enabled;
			}
		}
		if(dbgLight != null)
		{
			dbgLight.enabled = debugMode && powerOn;
		}
		if(!halted && powerOn && !debugMode)
		{
			if(haltTime>0)
			{
				haltTime-=1;
			}
			else
			{
				if(counter >= ram.Length)
				{
					halted = true;
				}
				else
				{
					RunTick(intC, true);
				}
			}
		}
	}

	public void RunTick(int numTimes, bool clearList)
	{
		for(int timesRunTick = 0; timesRunTick<numTimes; timesRunTick++) //Used to support processor speeds > 180Hz!
		{
			if(clearList)
			{
				runAddresses.Clear();
			}
			runAddresses.Add(counter);
			int ramTemp = ram[counter];
			int operation = (ramTemp >> 28) & 0xf; //4 opcode bits
			int value = ramTemp & 0xFFFFFFF; //28 value bits
			//Debug.Log("OP: "+operation+", VAL: "+value+", BIT: "+Convert.ToString(ramTemp, 2).PadLeft(32, '0')+", VALBIT: "+Convert.ToString(value, 2).PadLeft(28, '0'));
			if(operation == 0x0) //NIL
			{
				counter = counter + 1;
			}
			else if(operation == 0x1) //HLT [ticks]
			{
				int ticks = value & 0xFFFFFFF;
				if(ticks > 0)
				{
					haltTime = ticks;
					counter += 1;
				}
				else
				{
					halted = true;
				}
			}
			else if(operation == 0x2) //MOVI <register = 4b> <source = 24b>
			{
				int register = (value >> 24) & 0xF;
				int source = value & 0xFFFFFF;
				registers[register] = ram[source+globalOffset];
				counter += 1;
			}
			else if(operation == 0x3) //MOVO <register = 4b> <destination = 24b>
			{
				int register = (value >> 24) & 0xF;
				int destination = value & 0xFFFFFF;
				ram[destination+globalOffset] = registers[register];
				counter += 1;
				//Debug.Log("MOVO: "+register+", DEST: "+destination+", REGISTER: "+registers[register]+", RegisterBit: "+Convert.ToString(registers[register], 2)+", RAM: "+ram[destination]+", RAMBIT: "+Convert.ToString(ram[destination], 2));
			}
			else if(operation == 0x4) //JMP <flag = 2b> <destination = 24b> <null data 2b>
			{
				int flag = (value >> 26) & 0x3;
				int destination = (value >> 2) & 0xFFFFFF;
				if(flag == 0x0)
				{
					counter = counter + destination;
				}
				if(flag == 0x1)
				{
					counter = destination;
				}
				if(flag == 0x2)
				{
					counter = counter - destination;
				}
				if(flag == 0x3)
				{
					counter = (ram.Length - 1) - destination;
				}
				if(!runAddresses.Contains(counter))
				{
					RunTick(1, false);
				}
				else
				{
					halted = true;
					//do a flameout.  one day.
				}
			}
			else if(operation == 0x5) //SETDATA <port = 4b> <flag = 2b> <data for flag = 22b>
			{
				int port = (value >> 24) & 0xF;
				int flag = (value >> 22) & 0x3;
				int destination = value & 0x3FFFFF;
				int dataOut = destination;
				bool ext = false;
				int flag3 = 0;
				if(flag == 0)
				{
					string tempBits = Convert.ToString(destination, 2).PadLeft(22, '0').PadRight(32, '0');
					dataOut = Convert.ToInt32(tempBits, 2);
				}
				else if(flag == 1)
				{
					int flag2 = (destination >> 21) & 0x1;
					int address = destination & 0x1FFFFF;
					if(flag2 == 0)
					{
						dataOut = ram[counter - address];
					}
					else
					{
						dataOut = ram[counter + address];
					}
				}
				else if(flag == 2)
				{
					int flag2 = (destination >> 21) & 0x1;
					int address = destination & 0x1FFFFF;
					if(flag2 == 0)
					{
						dataOut = ram[address+globalOffset];
					}
					else
					{
						dataOut = ram[ram.Length - (address+globalOffset) - 1];
					}
				}
				else if(flag == 3)
				{
					int register = (destination >> 18) & 0xF;
					dataOut = registers[register];
					ext = ((destination >> 17) & 0x1) == 1;
					if(ext)
					{
						flag3 = (destination >> 13) & 0xF;
					}
					//Debug.Log(register);
				}
				//Debug.Log("PORT: "+port+", DataOut: "+dataOut+", FLAG: "+flag+", DESTINATION: "+destination);
				if(!ext)
					ports[port].SetData(dataOut);
				else
					ports[port].ExtendedSetData(dataOut, registers[flag3]);
				counter += 1;
			}
			else if(operation == 0x6) //GETDATA <port = 4b> <flag = 2b> <data for flag = 22b>
			{
				int port = (value >> 24) & 0xF;
				int flag = (value >> 22) & 0x3;
				int destination = value & 0x3FFFFF;
				int dataOut = destination;
				if(flag == 0)
				{
					string tempBits = Convert.ToString(destination, 2).PadLeft(22, '0').PadRight(32, '0');
					dataOut = Convert.ToInt32(tempBits, 2);
				}
				else if(flag == 1)
				{
					int flag2 = (destination >> 21) & 0x1;
					int address = destination & 0x1FFFFF;
					if(flag2 == 0)
					{
						dataOut = ram[counter - address];
					}
					else
					{
						dataOut = ram[counter + address];
					}
				}
				else if(flag == 2)
				{
					int flag2 = (destination >> 21) & 0x1;
					int address = destination & 0x1FFFFF;
					if(flag2 == 0)
					{
						dataOut = ram[address+globalOffset];
					}
					else
					{
						dataOut = ram[ram.Length - (address+globalOffset) - 1];
					}
				}
				else if(flag == 3)
				{
					int register = (destination >> 18) & 0xF;
					dataOut = registers[register];
					//Debug.Log(register);
				}
				//Debug.Log("PORT: "+port+", DataOut: "+dataOut+", FLAG: "+flag+", DESTINATION: "+destination);
				registers[1] = ports[port].GetData(dataOut);
				//Debug.Log(registers[1]);
				counter += 1;
			}
			else if(operation == 0x7) //SET <register = 4b> <startPos = 2b> <data = 8b>
			{
				int register = (value >> 24) & 0xF;
				int myData = registers[register];
				int bNum = (value >> 22) & 0x3;
				int val = (value >> 14) & 0xFF;
				string bit = Convert.ToString(myData, 2);
				if(bit.Length < 32)
				{
					bit = bit.PadLeft(32, '0');
				}
				if(bNum == 0)
				{
					bit = bit.Remove(0, 8);
					bit = (Convert.ToString(val, 2).PadLeft(8, '0')) + bit;
				}
				if(bNum == 1)
				{
					bit = bit.Remove(8, 8);
					bit = bit.Insert(8, (Convert.ToString(val, 2).PadLeft(8, '0')));
				}
				if(bNum == 2)
				{
					bit = bit.Remove(16, 8);
					bit = bit.Insert(16, (Convert.ToString(val, 2).PadLeft(8, '0')));
				}
				if(bNum == 3)
				{
					bit = bit.Remove(24, 8);
					bit = bit + (Convert.ToString(val, 2).PadLeft(8, '0'));
				}
				registers[register] = Convert.ToInt32(bit, 2);
				;
				counter += 1;
			}
			else if(operation == 0x8) //IFJMP <flag = 2b> <address = 24b> <operation = 2b>
			{
				int flag = (value >> 26) & 0x3;
				int destination = (value >> 2) & 0xFFFFFF;
				int myData = 0;
				if(flag == 0x0)
				{
					myData = counter + destination;
				}
				if(flag == 0x1)
				{
					myData = destination;
				}
				if(flag == 0x2)
				{
					myData = counter - destination;
				}
				if(flag == 0x3)
				{
					myData = (ram.Length - 1) - destination;
				}
				int operationIF = value & 0x3;
				bool doJump = false;
				if(operationIF == 0x0)
				{
					doJump = registers[2] == registers[3];
				}
				else if(operationIF == 0x1)
				{
					doJump = registers[2] != registers[3];
				}
				else if(operationIF == 0x2)
				{
					doJump = registers[2] > registers[3];
				}
				else if(operationIF == 0x3)
				{
					doJump = registers[2] < registers[3];
				}
				if(doJump)
				{
					counter = myData;
				}
				else
				{
					counter += 1;
				}
				if(!runAddresses.Contains(counter))
				{
					RunTick(1, false);
				}
				else
				{
					halted = true;
					//do a flameout.  one day.
				}
			}
			else if(operation == 0x9) //PMOV <registry source = 4b> <registry target = 4b> <start = 5b> <end = 5b> <shift = 5b> <offsetNeg = 1b>
			{
				int registrySource = (value >> 24) & 0xF;
				//Debug.Log(Convert.ToString(registrySource, 2));
				int registryTarget = (value >> 20) & 0xF;
				//Debug.Log(Convert.ToString(registryTarget, 2));
				int myData = registers[registrySource];
				int start = (value >> 15) & 0x1F;
				int end = (value >> 10) & 0x1F;
				int offset = (value >> 5) & 0x1F;
				bool offsetNeg = ((value >> 4) & 0x1) == 0;
				offset = offsetNeg ? -offset : offset;
				//Debug.Log("PMOV, START: "+start+", END: "+end+", OFFSET: "+offset+", OVERWRITE: "+overWrite);
				string bitRead = Convert.ToString(myData, 2).PadLeft(32, '0');
				string bit = Convert.ToString(registers[registryTarget], 2);
				bit = bit.PadLeft(32, '0');
				//Debug.Log("PMOV, BIT: "+bit+", BITREAD: "+bitRead);
				char[] temp = bit.ToCharArray();
				for(int i = start + (offset != 0 ? 0 : 0); i < end + 1; i++)
				{
					//bit.Remove(i);
					//bit.Insert(i, ""+);
					int ti = i;
					if(offset != 0)
					{
						ti += offset;
					}
					if(ti >= 32)
					{
						ti -= 32;
					}
					if(ti < 0)
					{
						ti = 32 + ti;
					}
					//Debug.Log("I: " + i + ", TI: " + ti);
					//Debug.Log("temp: " + temp[ti]+", tempI: "+temp[i]+", bitRead: "+bitRead[i]);
					temp[ti] = bitRead[i];
				}
				bit = new string(temp);
				//Debug.Log("PMOV, RegS: "+registrySource+", RegT: "+registryTarget+", myData: "+myData+", start: "+start+", end: "+end+", offset: "+offset+", offsetNeg: "+offsetNeg);
				//Debug.Log("PMOV, BIT AFTER MODIFICATION: "+bit);
				registers[registryTarget] = Convert.ToInt32(bit, 2);
				counter += 1;
			}
			else if(operation == 0xA) //MATH <registry source = 4b> <registry target = 4b> <operation = 4b> <extra data = 14b>
			{
				int registrySource = (value >> 24) & 0xF;
				int registryTarget = (value >> 20) & 0xF;
				int mathOperation = (value >> 16) & 0xF;
				int extra = value & 0xFF;
				int valueSource = registers[registrySource];
				int valueTarget = registers[registryTarget];
				if(mathOperation == 0)
				{
					registers[registryTarget] = valueTarget + valueSource;
				}
				else if(mathOperation == 1)
				{
					registers[registryTarget] = valueTarget - valueSource;
				}
				else if(mathOperation == 2)
				{
					registers[registryTarget] = valueTarget * valueSource;
				}
				else if(mathOperation == 3)
				{
					registers[registryTarget] = valueTarget / valueSource;
				}
				else if(mathOperation == 4)
				{
					registers[registryTarget] = valueTarget % valueSource;
				}
				else if(mathOperation == 5)
				{
					registers[registryTarget] = valueSource;
				}
				else if(mathOperation == 6)
				{
					registers[registryTarget] = UnityEngine.Random.Range(0, valueSource);
				}
				counter += 1;
			}
			else if(operation == 0xB) //UTIL <sub-code = 4b> <arguments = 24b>
			{
				int subcode = (value >> 24) & 0xF;
				int arguments = value & 0xFFFFFF;
				if(subcode == 0)
				{
					globalOffset = arguments;
					counter += 1;
				}
			}
		}
	}
}