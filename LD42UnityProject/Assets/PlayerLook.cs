﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Linq;

/// <summary>
/// A class for representing individual tutorial goals.
/// </summary>
[System.Serializable]
public class TutorialGoal
{
	/// <summary>
	/// General description of this goal.
	/// </summary>
	[Multiline(5)]
	public string goalInfo;
	/// <summary>
	/// Hints for the goal.
	/// </summary>
	[Multiline(5)]
	public string[] goalHints;
	/// <summary>
	/// Computer state.
	/// </summary>
	public TutorialCompState compyS;
	/// <summary>
	/// Drive state.
	/// </summary>
	public TutorialDriveState driveS;
	/// <summary>
	/// Monitor state.
	/// </summary>
	public TutorialMonitorState monitS;
	/// <summary>
	/// Player state.
	/// </summary>
	public TutorialPlayerState playrS;
	/// <summary>
	/// If this was the last step in the tutorial, it sets up a WIN message.
	/// </summary>
	public bool lastStep;
	/// <summary>
	/// If this goal can "fail" and jump you backwards.
	/// </summary>
	public float timeTillFail = -1;
	/// <summary>
	/// Where to jump back to.
	/// </summary>
	public int failJump;
}

/// <summary>
/// Represents a few important player stats, like selected object, mouse state, etc.
/// </summary>
[System.Serializable]
public class TutorialPlayerState
{
	/// <summary>
	/// Is the mouse supposed to be captured?
	/// </summary>
	public bool mouseCaptured;
	/// <summary>
	/// Do we care if the mouse is captured?
	/// </summary>
	public bool careMouse;
	/// <summary>
	/// What object is supposed to be selected?
	/// </summary>
	public GameObject selected;
	/// <summary>
	/// Do we care if it's selected or not?
	/// </summary>
	public bool careSelected;
	/// <summary>
	/// Should the assembler's current code contents look like this?
	/// </summary>
	[Multiline(5)]
	public string asmCode;
	/// <summary>
	/// Should the assembler's current program name look like this?
	/// </summary>
	public string asmName;
	/// <summary>
	/// Do we care about the assembler data?
	/// </summary>
	public bool careAsm,careName;
}

/// <summary>
/// Represents the current state of a computer for tutorial purposes.
/// </summary>
[System.Serializable]
public class TutorialCompState
{
	/// <summary>
	/// Should RAM look like this at a specified point?
	/// </summary>
	public int[] ramGoal;
	/// <summary>
	/// Is the RAM goal mandatory?
	/// </summary>
	public bool careRam;
	/// <summary>
	/// Should the ROM look like this at a specified point?
	/// </summary>
	public int[] romGoal;
	/// <summary>
	/// Is the ROM goal mandatory?
	/// </summary>
	public bool careRom;
	/// <summary>
	/// Should the registers look like this at a specified point?
	/// </summary>
	public int[] registerGoal = new int[16];
	/// <summary>
	/// Is the register goal mandatory?
	/// </summary>
	public bool careRegister;
	/// <summary>
	/// Do we care about the current power-on/off state of the computer?
	/// </summary>
	public bool powerGoal,carePower;
	/// <summary>
	/// Do we care if the computer is currently halted?
	/// </summary>
	public bool haltGoal,careHalt;
}

/// <summary>
/// Represents the current state of a harddrive for tutorial purposes.
/// </summary>
[System.Serializable]
public class TutorialDriveState
{
	/// <summary>
	/// Should the drive's storage state look like this at a specified point?
	/// </summary>
	public int[] driveStoreGoal;
	/// <summary>
	/// Is the drive's state mandatory?
	/// </summary>
	public bool careDrive;
}

/// <summary>
/// Represents the current state of a monitor for tutorial purposes.
/// </summary>
[System.Serializable]
public class TutorialMonitorState
{
	/// <summary>
	/// Used for getting if the monitor is displaying a specific "image".
	/// </summary>
	public int[] monColX,monColY,monCol;
	/// <summary>
	/// Is the monitor state mandatory?
	/// </summary>
	public bool careColors;
}

[System.Serializable]
/// <summary>
/// A class for handling in-game tutorials.
/// </summary>
public class Tutorial
{
	/// <summary>
	/// Name of the currently-active tutorial.
	/// </summary>
	public string name;
	/// <summary>
	/// A list of goals for this tutorial.
	/// </summary>
	public TutorialGoal[] goals;
	/// <summary>
	/// Compared against goals.
	/// </summary>
	public Computer tutorialComp;
	/// <summary>
	/// Compared against goals.
	/// </summary>
	public PortDrive tutorialDrive;
	/// <summary>
	/// Compared against goals.
	/// </summary>
	public PortScreen tutorialScreen;
	/// <summary>
	/// The current step in the tutorial.
	/// </summary>
	public int currentStep;
	/// <summary>
	/// The current tutorial info to display.
	/// </summary>
	public string currentInfo;
	/// <summary>
	/// The number of hints used.
	/// </summary>
	public int numHints;
	/// <summary>
	/// The current list of shown hints.
	/// </summary>
	public string currentHints;
	/// <summary>
	/// Used for timed goals.
	/// </summary>
	public float failTime = -1f;
}

public class PlayerLook : MonoBehaviour
{
	/// <summary>
	/// For looking around.
	/// </summary>
	public float lookSpeed,mouseX,mouseY;
	/// <summary>
	/// If a keyboard is selected, it goes here.
	/// </summary>
	public PortKeyboard keyboardInUse;
	/// <summary>
	/// If a mouse is selected, it goes here.
	/// </summary>
	public PortMouse mouseInUse;
	/// <summary>
	/// For selecting objects like a harddrive, or the computer.
	/// </summary>
	public GameObject selected;
	/// <summary>
	/// Used for the main Assembly window.
	/// </summary>
	public Rect window;
	/// <summary>
	/// Used for scrolling in the Assembly window.
	/// </summary>
	Vector2 scroll;
	/// <summary>
	/// The code IN the assembly window.
	/// </summary>
	public string code;
	/// <summary>
	/// The name of the program in the assembly window - used for saving/loading.
	/// </summary>
	public string programName;
	/// <summary>
	/// The name/path for the disk image.
	/// </summary>
	public string dmg;
	/// <summary>
	/// A style for bold labels.
	/// </summary>
	public GUIStyle boldLabel;
	/// <summary>
	/// The save file name in use right now.
	/// </summary>
	public string save;
	/// <summary>
	/// A list of tutorials available.
	/// </summary>
	public Tutorial[] tutorials;
	/// <summary>
	/// Is the debug window open?
	/// </summary>
	public bool debugOpen = false;
	//[System.Serializable]
	public GUIStyle style;
	//[System.Serializable]
	public GUIStyle styleWindow;

	public bool keyboardO = false;

	void Start()
	{
		//if(GameManager.instance != null)
		{
			save = GameManager.save;
		}
		/*Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;*/
		if(!Directory.Exists(Application.dataPath + "/../" + save))
		{
			Directory.CreateDirectory(Application.dataPath + "/../" + save);
		}
	}

	Tutorial t;

	void OnGUI()
	{
		window = GUI.Window(0, window, DrawASMWindow, "ASSEMBLER: Left-Click to select stuff!", styleWindow);
		if(GameManager.tutorialID != -1)
		{
			t = tutorials[GameManager.tutorialID];
			windowT = GUI.Window(2, windowT, DrawTUTWindow, "TUTORIAL: " + t.name, styleWindow);
		}
		if(debugOpen && !GameManager.LUDUM_DARE)
		{
			windowD = GUI.Window(3, windowD, DrawDBGWindow, "DEBUGGER TOOL", styleWindow);
		}
	}

	public Rect windowT;
	public Vector2 scrollT,scrollI,scrollH;

	public Rect windowD;
	public Vector2 scrollD;
	int ramMin = -1,ramMax = -1, DramMin = -1, DramMax = -1, driveMin = -1, driveMax = -1, driveDMin = -1, driveDMax = -1;

	int con = 0;
	bool pad = false;
	public string[] disassembly = new string[0];
	public string[] disassemblyRegister = new string[0];
	public string[] disassemblyDrive = new string[0];

	/// <summary>
	/// Disassemble the code in the specified RAM.
	/// </summary>
	/// <param name="ram">Ram.</param>
	string[] Disassemble(int[] ram)
	{
		List<string> lines = new List<string>();
		for(int i = 0; i < ram.Length; i++)
		{
			string assembled = "";
			int operation = (ram[i] >> 28) & 0xf; //4 opcode bits
			int value = ram[i] & 0xFFFFFFF; //28 value bits
			if(operation == 0 || operation > 10) //NIL or VARIABLE
			{
				if(ram[i] == 0)
					assembled += "NIL";
				else
					assembled += "DATAC " + (Convert.ToString(ram[i], 2).PadLeft(32, '0'));
			}
			else if(operation == 1) //HLT
			{
				int ticks = value & 0xFFFFFFF;
				assembled += "HLT";
				if(ticks > 0)
				{
					assembled += " " + ticks;
				}
			}
			else if(operation == 2 || operation == 3) //MOVI OR MOVO
			{
				int register = (value >> 24) & 0xF;
				int source = value & 0xFFFFFF;
				if(operation == 2)
					assembled += "MOVI ";
				else
					assembled += "MOVO ";
				assembled += register + " " + source;
			}
			else if(operation == 4) //JMP
			{
				int flag = (value >> 26) & 0x3;
				int destination = (value >> 2) & 0xFFFFFF;
				assembled += "JMP " + flag + " " + destination;
			}
			else if(operation == 5) //SETDATA
			{
				int port = (value >> 24) & 0xF;
				int flag = (value >> 22) & 0x3;
				int destination = value & 0x3FFFFF;
				int dataOut = destination;
				bool ext = false;
				int flag3 = 0;
				assembled += "SETDATA " + port + " " + flag;
				if(flag == 0)
				{
					assembled += " " + dataOut;
				}
				else if(flag == 1 || flag == 2)
				{
					int flag2 = (destination >> 21) & 0x1;
					int address = destination & 0x1FFFFF;
					assembled += " " + flag2 + " " + address;
				}
				else if(flag == 3)
				{
					int register = (destination >> 18) & 0xF;
					ext = ((destination >> 17) & 0x1) == 1;
					if(ext)
					{
						flag3 = (destination >> 13) & 0xF;
					}
					assembled += " " + register;
					if(ext)
					{
						assembled += " 1 " + flag3;
					}
				}
			}
			else if(operation == 6) //GETDATA
			{
				int port = (value >> 24) & 0xF;
				int flag = (value >> 22) & 0x3;
				int destination = value & 0x3FFFFF;
				int dataOut = destination;
				assembled += "GETDATA " + port + " " + flag;
				if(flag == 0)
				{
					assembled += " " + dataOut;
				}
				else if(flag == 1 || flag == 2)
				{
					int flag2 = (destination >> 21) & 0x1;
					int address = destination & 0x1FFFFF;
					assembled += " " + flag2 + " " + address;
				}
				else if(flag == 3)
				{
					int register = (destination >> 18) & 0xF;
					assembled += " " + register;
				}
			}
			else if(operation == 7) //SET
			{
				int register = (value >> 24) & 0xF;
				int bNum = (value >> 22) & 0x3;
				int val = (value >> 14) & 0xFF;
				assembled += "SET " + register + " " + bNum + " " + (Convert.ToString(val, 2).PadLeft(8, '0'));
			}
			else if(operation == 8) //IFJMP
			{
				int flag = (value >> 26) & 0x3;
				int destination = (value >> 2) & 0xFFFFFF;
				int operationIF = value & 0x3;
				assembled += "IFJMP " + flag + " " + destination + " " + operationIF;
			}
			else if(operation == 9) //PMOV
			{
				int registrySource = (value >> 24) & 0xF;
				int registryTarget = (value >> 20) & 0xF;
				int start = (value >> 15) & 0x1F;
				int end = (value >> 10) & 0x1F;
				int offset = (value >> 5) & 0x1F;
				int offsetNeg = (value >> 4) & 0x1;
				assembled += "PMOV " + registrySource + " " + registryTarget + " " + start + " " + end + " " + offset + " " + offsetNeg;
			}
			else if(operation == 10) //MATH
			{
				int registrySource = (value >> 24) & 0xF;
				int registryTarget = (value >> 20) & 0xF;
				int mathOperation = (value >> 16) & 0xF;
				int extra = value & 0xFF;
				assembled += "MATH " + registrySource + " " + registryTarget + " " + mathOperation;
				if(extra != 0)
				{
					assembled += " " + extra;
				}
			}
			else if(operation==11) //OFST
			{
				int subcode = (value >> 24) & 0xF;
				int arguments = value & 0xFF;
				if(subcode == 0)
				{
					assembled += "OFST " + arguments;
				}
			}
			lines.Add(assembled);
		}
		return lines.ToArray();
	}

	bool showRegisters =  false, registersInt = false, showRegistersD = false, driveInt = false;

	void DrawDBGWindow(int id)
	{
		scrollD = GUILayout.BeginScrollView(scrollD, style);
		GUILayout.Label("==Convert integer to bits==", boldLabel);
		con = int.Parse(GUILayout.TextField("" + con, style));
		GUI.backgroundColor = pad ? Color.red : Color.white;
		pad = GUILayout.Toggle(pad, "Pad to full length?", style);
		GUI.backgroundColor = Color.white;
		if(pad)
			GUILayout.TextField(Convert.ToString(con, 2).PadLeft(32, '0'), style);
		else
			GUILayout.TextField(Convert.ToString(con, 2), style);
		if(selected==null)
		{
			GUILayout.Label("NO SELECTED OBJECT!", boldLabel);
		}
		else if(selected.GetComponent<Computer>()!=null)
		{
			GUILayout.Label("COMPUTER SELECTED!", boldLabel);
			Computer compy = selected.GetComponent<Computer>();
			GUI.backgroundColor = Color.white;
			if(compy.debugMode)
				GUI.backgroundColor = Color.yellow;
			compy.debugMode = GUILayout.Toggle(compy.debugMode, "TOGGLE DEBUG MODE", style);
			GUI.backgroundColor = Color.white;
			if(compy.debugMode)
			{
				if(GUILayout.Button("STEP", style))
				{
					if(compy.haltTime>0)
					{
						compy.haltTime-=1;
					}
					else
					{
						if(compy.counter >= compy.ram.Length)
						{
							compy.halted = true;
						}
						else
						{
							compy.RunTick(1, true);
						}
					}
				}
			}
			GUILayout.Label("COUNTER LOC: "+compy.counter, style);
			GUILayout.Label("OFFSET: "+compy.globalOffset, style);
			GUILayout.Label("RAM VIEW", boldLabel);
			GUI.backgroundColor = showRegisters ? Color.green : Color.white;
			showRegisters = GUILayout.Toggle(showRegisters, "Show registers?", style);
			GUI.backgroundColor = Color.white;
			if(showRegisters)
			{
				GUI.backgroundColor = registersInt ? Color.green : Color.white;
				registersInt = GUILayout.Toggle(registersInt, "Show in int form?", style);
				GUI.backgroundColor = Color.white;
				for(int i = 0; i < compy.registers.Length; i++)
				{
					if(!registersInt)
					{
						string t = GUILayout.TextField(("" + i).PadLeft(2, '0') + ":" + Convert.ToString(compy.registers[i], 2).PadLeft(32, '0'), style);
						t = t.Substring((("" + i).PadLeft(2, '0') + ":").Length);
						compy.registers[i] = Convert.ToInt32(t, 2);
					}
					else
					{
						string t = GUILayout.TextField(("" + i).PadLeft(2, '0') + ":" + compy.registers[i], style);
						t = t.Substring((("" + i).PadLeft(2, '0') + ":").Length);
						compy.registers[i] = int.Parse(t);
					}
				}
			}
			GUILayout.TextArea("WORD OF WARNING: This lets you edit the RAM in REALTIME.\n\nIt provides a constant view of each memory address in bit form, highlighting the current address.  This means you can edit any address' contents anytime, anywhere.\n\nThe two boxes below are the minimum address & maximum address to view.\nRAM CAPACITY: "+compy.ram.Length+" addresses.", style);
			if(ramMin == -1 || ramMax == -1)
			{
				ramMin = 0;
				ramMax = compy.ram.Length;
			}
			ramMin = int.Parse(GUILayout.TextField("" + ramMin, style));
			ramMax = Mathf.Clamp(int.Parse(GUILayout.TextField("" + (ramMax-1), style)), ramMin, ramMin+31)+1;
			if(ramMax > compy.ram.Length)
				ramMax = compy.ram.Length;
			for(int i = ramMin; i < ramMax; i++)
			{
				GUI.backgroundColor = compy.counter==i ? Color.blue : Color.white;
				string t = GUILayout.TextField((""+i).PadLeft(7, '0') + ":" + Convert.ToString(compy.ram[i], 2).PadLeft(32, '0'), style);
				t = t.Substring(((""+i).PadLeft(7, '0') + ":").Length);
				//Debug.Log(t);
				compy.ram[i] = Convert.ToInt32(t, 2);
			}
			GUI.backgroundColor = Color.white;
			GUILayout.Label("DISASSEMBLY", boldLabel);
			if(GUILayout.Button("DISASSEMBLE", style))
			{
				disassembly = Disassemble(compy.ram);
				DramMin = 0;
				DramMax = disassembly.Length;
				disassemblyRegister = Disassemble(compy.registers);
			}
			if(GUILayout.Button("CLEAR", style))
			{
				disassembly = new string[0];
				DramMin = 0;
				DramMax = 0;
				disassemblyRegister = new string[0];
			}
			if(DramMin == -1 || DramMax == -1)
			{
				DramMin = 0;
				DramMax = disassembly.Length;
			}
			GUI.backgroundColor = showRegisters ? Color.green : Color.white;
			showRegistersD = GUILayout.Toggle(showRegistersD, "Show (disassembled) registers?", style);
			GUI.backgroundColor = Color.white;
			if(showRegistersD)
			{
				for(int i = 0; i < disassemblyRegister.Length; i++)
				{
					GUILayout.TextField(("" + i).PadLeft(2, '0') + ":" + disassemblyRegister[i], style);
				}
			}
			DramMin = int.Parse(GUILayout.TextField("" + DramMin, style));
			DramMax = Mathf.Clamp(int.Parse(GUILayout.TextField("" + (DramMax-1), style)), DramMin, DramMin+31)+1;
			if(DramMax > disassembly.Length)
				DramMax = disassembly.Length;
			for(int i = DramMin; i < DramMax; i++)
			{
				GUI.backgroundColor = compy.counter==i ? Color.blue : Color.white;
				GUILayout.TextField(("" + i).PadLeft(7, '0') + ":" + disassembly[i], style);
			}
			GUI.backgroundColor = Color.white;
		}
		else if(selected.GetComponent<PortDrive>()!=null)
		{
			GUILayout.Label("DISK DRIVE SELECTED!", boldLabel);
			PortDrive drive = selected.GetComponent<PortDrive>();
			GUILayout.Label("DATA VIEWER", boldLabel);
			GUILayout.TextArea("WORD OF WARNING: This lets you edit the drive's storage in REALTIME.\n\nIt provides a constant view of each memory address in bit form, or int form.  This means you can edit any address' contents anytime, anywhere.\n\nThe two boxes below are the minimum address & maximum address to view.\nDISK CAPACITY: "+drive.driveStorage.Length+" addresses.", style);
			if(driveMin == -1 || driveMax == -1)
			{
				driveMin = 0;
				driveMax = drive.driveStorage.Length;
			}
			driveMin = int.Parse(GUILayout.TextField("" + driveMin, style));
			driveMax = Mathf.Clamp(int.Parse(GUILayout.TextField("" + (driveMax-1), style)), driveMin, driveMin+31)+1;
			if(ramMax > drive.driveStorage.Length)
				ramMax = drive.driveStorage.Length;
			GUI.backgroundColor = driveInt ? Color.red : Color.white;
			registersInt = GUILayout.Toggle(driveInt, "Show in int form?", style);
			GUI.backgroundColor = Color.white;
			for(int i = driveMin; i < driveMax; i++)
			{
				if(!driveInt)
				{
					string t = GUILayout.TextField(("" + i).PadLeft(7, '0') + ":" + Convert.ToString(drive.driveStorage[i], 2).PadLeft(32, '0'), style);
					t = t.Substring((("" + i).PadLeft(7, '0') + ":").Length);
					drive.driveStorage[i] = Convert.ToInt32(t, 2);
				}
				else
				{
					string t = GUILayout.TextField(("" + i).PadLeft(7, '0') + ":" + drive.driveStorage[i], style);
					t = t.Substring((("" + i).PadLeft(7, '0') + ":").Length);
					drive.driveStorage[i] = int.Parse(t);
				}
			}
			GUI.backgroundColor = Color.white;
			GUILayout.Label("DISASSEMBLY", boldLabel);
			if(GUILayout.Button("DISASSEMBLE", style))
			{
				disassemblyDrive = Disassemble(drive.driveStorage);
				driveDMin = 0;
				driveDMax = disassemblyDrive.Length;
			}
			if(GUILayout.Button("CLEAR", style))
			{
				disassemblyDrive = new string[0];
				driveDMin = 0;
				driveDMax = 0;
			}
			if(DramMin == -1 || DramMax == -1)
			{
				driveDMin = 0;
				driveDMax = disassemblyDrive.Length;
			}
			driveDMin = int.Parse(GUILayout.TextField("" + driveDMin, style));
			driveDMax = Mathf.Clamp(int.Parse(GUILayout.TextField("" + (driveDMax-1), style)), driveDMin, driveDMin+31)+1;
			if(driveDMax > disassemblyDrive.Length)
				driveDMax = disassemblyDrive.Length;
			for(int i = driveDMin; i < driveDMax; i++)
			{
				if(i>0 && i<disassemblyDrive.Length)
					GUILayout.TextField(("" + i).PadLeft(7, '0') + ":" + disassemblyDrive[i], style);
			}
			GUI.backgroundColor = Color.white;
		}
		else
		{
			GUILayout.Label("SELECTED OBJECT NOT COMPUTER OR DRIVE!", boldLabel);
		}
		GUILayout.Label("MISC", boldLabel);
		if(GUILayout.Button("CLOSE DEBUGGER", style))
		{
			debugOpen = false;
		}
		GUILayout.EndScrollView();
		GUI.DragWindow();
	}

	void DrawTUTWindow(int id)
	{
		scrollT = GUILayout.BeginScrollView(scrollT, style, GUILayout.ExpandWidth(false));
		TutorialGoal tg = t.goals[t.currentStep];
		if(t.currentInfo == "")
		{
			t.currentInfo = tg.goalInfo;
			t.currentHints = "";
		}
		scrollI = GUILayout.BeginScrollView(scrollI, style, GUILayout.MinHeight(150), GUILayout.MaxHeight(150), GUILayout.ExpandHeight(false), GUILayout.ExpandWidth(false));
		GUILayout.TextArea(t.currentInfo, style);
		GUILayout.EndScrollView();
		GUILayout.Space(10);
		scrollH = GUILayout.BeginScrollView(scrollH, style, GUILayout.MinHeight(300), GUILayout.MaxHeight(300), GUILayout.ExpandHeight(false), GUILayout.ExpandWidth(false));
		GUILayout.TextArea(t.currentHints, style);
		GUILayout.EndScrollView();
		GUILayout.Space(10);
		string hintT = "GET HINT?";
		if(!(t.numHints < tg.goalHints.Length))
		{
			hintT = "OUT OF INFO ON THIS STEP";
		}
		if(GUILayout.Button(hintT, style) && t.numHints<tg.goalHints.Length)
		{
			t.currentHints += tg.goalHints[t.numHints] + "\n\n";
			t.numHints += 1;
		}
		bool pass = true;
		if(tg.playrS.careMouse && !(Cursor.visible != tg.playrS.mouseCaptured))
		{
			pass = false;
		}
		if(tg.playrS.careSelected && !(selected == tg.playrS.selected))
		{
			pass = false;
		}
		if(tg.playrS.careAsm && !(code == tg.playrS.asmCode))
		{
			pass = false;
		}
		if(tg.playrS.careName && !(code == tg.playrS.asmName))
		{
			pass = false;
		}
		if(tg.compyS.careRam && !(t.tutorialComp.ram.SequenceEqual(tg.compyS.ramGoal)))
		{
			pass = false;
		}
		if(tg.compyS.careRom && !(t.tutorialComp.rom.SequenceEqual(tg.compyS.romGoal)))
		{
			pass = false;
		}
		if(tg.compyS.careRegister && !(t.tutorialComp.registers.SequenceEqual(tg.compyS.registerGoal)))
		{
			pass = false;
		}
		if(tg.compyS.carePower && !(t.tutorialComp.powerOn == tg.compyS.powerGoal))
		{
			pass = false;
		}
		if(tg.compyS.careHalt && !((t.tutorialComp.haltTime > 0 || t.tutorialComp.halted) == tg.compyS.haltGoal))
		{
			pass = false;
		}
		if(tg.monitS.careColors)
		{
			//bool tmp = true;
			for(int i = 0; i < tg.monitS.monCol.Length; i++)
			{
				if(t.tutorialScreen.colSets[tg.monitS.monColX[i], tg.monitS.monColY[i]]!=tg.monitS.monCol[i])
				{
					pass = false;
				}
			}
		}
		if(t.failTime != -1f)
		{
			t.failTime -= Time.deltaTime;
			if(t.failTime <= 0)
			{
				t.currentStep = tg.failJump;
				tg = t.goals[t.currentStep];
				t.currentInfo = tg.goalInfo;
				t.currentHints = "";
				t.numHints = 0;
				t.failTime = tg.timeTillFail;
			}
		}
		if(pass)
		{
			t.currentStep += 1;
			if(tg.lastStep)
			{
				GameManager.SetTutorialsFinished(true, GameManager.tutorialID + 1); //0-indexed
			}
			tg = t.goals[t.currentStep];
			t.currentInfo = tg.goalInfo;
			t.currentHints = "";
			t.numHints = 0;
			t.failTime = tg.timeTillFail;
		}
		GUILayout.EndScrollView();
		GUI.DragWindow();
	}

	/// <summary>
	/// Draw the Assembly window, used for writing & compiling code to ROM and drive.
	/// </summary>
	/// <param name="id">Identifier.</param>
	void DrawASMWindow(int id)
	{
		//Initialize scroll view & such.
		scroll = GUILayout.BeginScrollView(scroll, style);
		//Code.
		GUILayout.Label("CODE INFO", boldLabel);
		programName = GUILayout.TextField(programName, style);
		code = GUILayout.TextArea(code, style);
		//Saving and loading said code to disk.
		GUILayout.Label("SAVE/LOAD", boldLabel);
		if(GUILayout.Button("Save", style))
		{
			StreamWriter sw = new StreamWriter(Application.dataPath+"/../"+save+"/"+programName+".casm");
			sw.Write(code);
			sw.Close();
		}
		if(GUILayout.Button("Load", style))
		{
			StreamReader sr = new StreamReader(Application.dataPath+"/../"+save+"/"+programName+".casm");
			code = sr.ReadToEnd();
			sr.Close();
		}
		//Compiling the code, including installing to ROM/drive.
		GUILayout.Label("COMPILE", boldLabel);
		if(GUILayout.Button("Compile", style))
		{
			compiled = Compile();
		}
		if(selected==null)
		{
			GUILayout.Label("NO SELECTED OBJECT!", boldLabel);
		}
		else if(selected.GetComponent<Computer>()!=null)
		{
			if(GUILayout.Button("Compile to ROM", style))
			{
				selected.GetComponent<Computer>().rom = compiled;
			}
		}
		else if(selected.GetComponent<PortDrive>()!=null)
		{
			PortDrive drive = selected.GetComponent<PortDrive>();
			if(GUILayout.Button("Clear HDD: DANGEROUS", style))
			{
				for(int i = 0; i<drive.driveStorage.Length; i++)
				{
					drive.driveStorage[i] = 0;
				}
			}
			if(compiled!=null)
			{
				if(compiled.Length > drive.driveStorage.Length)
				{
					GUILayout.Label("DRIVE TOO SMALL!  NEEDS " + (drive.driveStorage.Length - compiled.Length) + " MORE ADDRESSES OF STORAGE!", boldLabel);
				}
				else
				{
					if(GUILayout.Button("Install", style))
					{
						for(int i = 0; i < compiled.Length; i++)
						{
							drive.driveStorage[i] = compiled[i];
						}
					}
				}
			}
			//Save and load the raw disk image.
			GUILayout.Label("SAVE/LOAD DMG", boldLabel);
			dmg = GUILayout.TextField(dmg, style);
			if(GUILayout.Button("Save Disk Image (plaintext-bits)", style))
			{
				StreamWriter sw = new StreamWriter(Application.dataPath+"/../"+save+"/"+dmg+".cdmgpt");
				string thing = "";
				for(int i = 0; i < drive.driveStorage.Length; i++)
				{
					thing += Convert.ToString(drive.driveStorage[i], 2).PadLeft(32, '0');
					if(i < drive.driveStorage.Length) //Add newlines until the last entry
						thing += "\n";
				}
				sw.Write(thing);
				sw.Close();
			}
			if(GUILayout.Button("Load Disk Image (plaintext-bits)", style))
			{
				StreamReader sr = new StreamReader(Application.dataPath+"/../"+save+"/"+dmg+".cdmgpt");
				string thing = sr.ReadToEnd();
				string[] lines = thing.Split('\n');
				for(int i = 0; i < lines.Length; i++)
				{
					drive.driveStorage[i] = Convert.ToInt32(lines[i], 2);
				}
				sr.Close();
			}
		}
		else
		{
			GUILayout.Label("SELECTED OBJECT NOT DRIVE OR PROCESSOR W/ ROM!", boldLabel);
		}
		//Only used for including a back-to-main-menu button, at the moment.
		GUILayout.Label("MISC. STUFF", boldLabel);
		if(GUILayout.Button("BACK", style))
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene("Scenes/MenuScene");
		}
		if(GUILayout.Button((debugOpen ? "CLOSE" : "OPEN")+" DEBUGGER", style))
		{
			debugOpen = !debugOpen;
		}
		GUILayout.Space(5);
		GUILayout.TextArea(console, style);
		GUILayout.EndScrollView();
		GUI.DragWindow();
	}

	/// <summary>
	/// The array of compiled bytecode.
	/// </summary>
	int[] compiled;
	/// <summary>
	/// An as-of-yet unused console for storing errors and such.
	/// </summary>
	public string console;

	/// <summary>
	/// Throw an exception in the console.
	/// </summary>
	/// <param name="exception">Exception.</param>
	void Except(string exception)
	{
		console += exception + "\n";
	}

	// Compile the program into bytecode.
	int[] Compile()
	{
		console = "";
		List<int> compiled = new List<int>();
		string[] listLines = code.Split('\n');
		foreach(string s in listLines)
		{
			if(!s.StartsWith("//")) //ignore comments
			{
				//BitArray bits = new BitArray(32); // initialize the bits used to set up the memory address
				string bits = "";
				if(s.StartsWith("DATAC")) //Set this memory location to be a specified 32-bit...thing.  Manually set bits, basically.
				{
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//") && remCA == -1)
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 2)
					{
						bits += s2[1];
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("NILLIST")) //Barf a specified number of NIL entries into memory.
				{
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 2)
					{
						bits += "00000000000000000000000000000000";
						int numParts = int.Parse(s2[1]);
						for(int i = 1; i < numParts; i++)
						{
							compiled.Add(0);
						}
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("NIL"))
				{
					bits += "00000000000000000000000000000000";
					//DO NOTHING, NIL IS 0x0000, TAKES NO ARGUMENTS, JUST WASTES A CLOCK CYCLE
				}
				else if(s.StartsWith("HLT")) // HLT [ticks] - halts the processor indefinitely if ticks==0/null, otherwise for the specified number of cycles.
				{
					bits += "0001";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 2)
					{
						string converted = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted.Length < 28)
						{
							converted = converted.PadLeft(28, '0');
						}
						if(converted.Length != 28)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  HALT TICK COUNT MUST BE LESS THAN OR EQUAL TO 28 BITS!  " + converted);
						}
						bits += converted;
					}
					else if(s2.Length == 1)
					{
						bits += "".PadLeft(28, '0');
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("MOVI")) //MOVI <register=4b> <address=24b> - Copy the contents of the specified RAM address into the specified Register.
				{
					bits += "0010";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 3)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length < 4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  REGISTER MUST BE LESS THAN OR EQUAL TO 2 BITS!" + converted1);
						}
						bits += converted1;
						string converted2 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted2.Length < 24)
						{
							converted2 = converted2.PadLeft(24, '0');
						}
						if(converted2.Length != 24)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  ADDRESS MUST BE LESS THAN OR EQUAL TO 12 BITS!" + converted2);
						}
						bits += converted2;
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("MOVO")) //MOVO <register=4b> <address=24b> - Copy the contents of the specified Register into the specified RAM address.
				{
					bits += "0011";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 3)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length < 4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  REGISTER MUST BE LESS THAN OR EQUAL TO 2 BITS!" + converted1);
						}
						bits += converted1;
						string converted2 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted2.Length < 24)
						{
							converted2 = converted2.PadLeft(24, '0');
						}
						if(converted2.Length != 24)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  ADDRESS MUST BE LESS THAN OR EQUAL TO 12 BITS!" + converted2);
						}
						bits += converted2;
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("JMP")) //JMP <flag = 2b> <address = 24b> [null data = 2b] - Jumps forwards, to a specified address, backwards, or backwards from the end of RAM by a specified amount.
				{
					bits += "0100";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 3)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length < 2)
						{
							converted1 = converted1.PadLeft(2, '0');
						}
						if(converted1.Length != 2)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  FLAG MUST BE LESS THAN OR EQUAL TO 2 BITS!" + converted1);
						}
						bits += converted1;
						string converted2 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted2.Length < 24)
						{
							converted2 = converted2.PadLeft(24, '0');
						}
						if(converted2.Length != 24)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  DESTINATION MUST BE LESS THAN OR EQUAL TO 26 BITS!" + converted2);
						}
						bits += converted2;
						bits += "00";
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("SETDATA")) //SETDATA <port = 4b> <flag = 2b> <data for flag = 22b> - Complicated, please see included documentation.
				{
					bits += "0101";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length >= 4)
					{
						bool extend = false;
						string converted = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted.Length < 4)
						{
							converted = converted.PadLeft(4, '0');
						}
						if(converted.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  PORT MUST BE LESS THAN OR EQUAL TO 4 BITS!" + converted);
						}
						bits += converted;
						converted = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted.Length < 2)
						{
							converted = converted.PadLeft(2, '0');
						}
						if(converted.Length != 2)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  FLAG MUST BE LESS THAN OR EQUAL TO 2 BITS!" + converted);
						}
						bits += converted;
						int flag = int.Parse(s2[2]);
						if(flag == 0)
						{
							bits += s2[3];
						}
						else if(flag == 1 || flag == 2)
						{
							int addr = int.Parse(s2[3]);
							bool neg = false;
							if(addr < 0)
							{
								addr = -addr;
								neg = true;
							}
							if(neg)
								bits += "0";
							else
								bits += "1";
							bits += Convert.ToString(addr, 2).PadLeft(21, '0');
						}
						else if(flag == 3)
						{
							int reg = int.Parse(s2[3]);
							if(s2.Length >= 5)
							{
								int reg2 = int.Parse(s2[4]);
								bits += Convert.ToString(reg, 2).PadLeft(4, '0');
								bits += "1";
								bits += Convert.ToString(reg2, 2).PadLeft(4, '0').PadRight(17, '0');
							}
							else
							{
								bits += Convert.ToString(reg, 2).PadLeft(4, '0').PadRight(22, '0');
							}
						}
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("GETDATA")) //GETDATA <port = 4b> <flag = 2b> <data for flag = 22b> - Complicated, please see included documentation.
				{
					bits += "0110";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 4)
					{
						string converted = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted.Length < 4)
						{
							converted = converted.PadLeft(4, '0');
						}
						if(converted.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  PORT MUST BE LESS THAN OR EQUAL TO 4 BITS!" + converted);
						}
						bits += converted;
						converted = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted.Length < 2)
						{
							converted = converted.PadLeft(2, '0');
						}
						if(converted.Length != 2)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  FLAG MUST BE LESS THAN OR EQUAL TO 2 BITS!" + converted);
						}
						bits += converted;
						int flag = int.Parse(s2[2]);
						if(flag == 0)
						{
							bits += s2[3];
						}
						else if(flag == 1 || flag == 2)
						{
							int addr = int.Parse(s2[3]);
							bool neg = false;
							if(addr < 0)
							{
								addr = -addr;
								neg = true;
							}
							if(neg)
								bits += "0";
							else
								bits += "1";
							bits += Convert.ToString(addr, 2).PadLeft(21, '0');
						}
						else if(flag == 3)
						{
							int reg = int.Parse(s2[3]);
							bits += Convert.ToString(reg, 2).PadLeft(4, '0').PadRight(22, '0');
						}
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("SET")) //SET <register = 4b> <byteNum = 2b> <byte = 8b> - Set one of four bytes stored in the specified register.  Registers contain 32 bits, just like all RAM memory addresses.
				{
					bits += "0111";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 4)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length < 4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!" + converted1);
						}
						bits += converted1;
						string converted2 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted2.Length < 2)
						{
							converted2 = converted2.PadLeft(2, '0');
						}
						if(converted2.Length != 2)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  BYTE POSITION MUST BE LESS THAN OR EQUAL TO 2 BITS!" + converted2);
						}
						bits += converted2;
						if(s2[3].Length == 8) //A sequence of 8 bits was specified
						{
							bits += s2[3];
						}
						else //A number was specified, parse it
						{
							bits += Convert.ToString(int.Parse(s2[3]), 2).PadLeft(8, '0');
						}
						bits += "00000000000000";
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("IFJMP")) //IFJMP <flag = 2b> <address = 24b> <operation = 2b> - Jump to a specified memory address if Register 2 and 3 match the specified operation, otherwise count forwards normally.  See documentation.
				{
					bits += "1000";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 4)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length < 2)
						{
							converted1 = converted1.PadLeft(2, '0');
						}
						if(converted1.Length != 2)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  FLAG MUST BE LESS THAN OR EQUAL TO 2 BITS!" + converted1);
						}
						bits += converted1;
						string converted2 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted2.Length < 24)
						{
							converted2 = converted2.PadLeft(24, '0');
						}
						if(converted2.Length != 24)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  ADDRESS MUST BE LESS THAN OR EQUAL TO 24 BITS!" + converted2);
						}
						bits += converted2;
						//bits+=s2[3];
						converted2 = Convert.ToString(int.Parse(s2[3]), 2);
						if(converted2.Length < 2)
						{
							converted2 = converted2.PadLeft(2, '0');
						}
						if(converted2.Length != 2)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  OPERATION MUST BE LESS THAN OR EQUAL TO 12 BITS!" + converted2);
						}
						bits += converted2;
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("PMOV")) //PMOV <source register = 4b> <target register = 4b> <start = 5b> <end = 5b> <shift = 5b> <shiftPos = 1b> - Copy a section of bits from the specified source register, and paste them in (optionally with a position offset) to the target.
				{
					bits += "1001";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 7)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length < 4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  SOURCE REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!" + converted1);
						}
						bits += converted1;
						converted1 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted1.Length < 4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  TARGET REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!" + converted1);
						}
						bits += converted1;
						converted1 = Convert.ToString(int.Parse(s2[3]), 2);
						if(converted1.Length < 5)
						{
							converted1 = converted1.PadLeft(5, '0');
						}
						if(converted1.Length != 5)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  START MUST BE LESS THAN OR EQUAL TO 5 BITS!" + converted1);
						}
						bits += converted1;
						converted1 = Convert.ToString(int.Parse(s2[4]), 2);
						if(converted1.Length < 5)
						{
							converted1 = converted1.PadLeft(5, '0');
						}
						if(converted1.Length != 5)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  END MUST BE LESS THAN OR EQUAL TO 5 BITS!" + converted1);
						}
						bits += converted1;
						string converted2 = Convert.ToString(int.Parse(s2[5]), 2);
						if(converted2.Length < 5)
						{
							converted2 = converted2.PadLeft(5, '0');
						}
						if(converted2.Length != 5)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  SHIFT MUST BE LESS THAN OR EQUAL TO 5 BITS!" + converted2);
						}
						bits += converted2;
						bits += s2[6]; //1 or 0
						bits += "0000";
						if(bits.Length != 32)
							Except("WTF WHY IS IT SHORT: " + bits);
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("MATH")) //MATH <source register = 4b> <target register = 4b> <operation = 4b> - Do an arbitrary math operation of some variety with the source & target registers..  See documentation.
				{
					bits += "1010";
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 5)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length < 4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  SOURCE REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!" + converted1);
						}
						bits += converted1;
						converted1 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted1.Length < 4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  TARGET REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!" + converted1);
						}
						bits += converted1;
						string converted2 = Convert.ToString(int.Parse(s2[3]), 2);
						if(converted2.Length < 4)
						{
							converted2 = converted2.PadLeft(4, '0');
						}
						if(converted2.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  OPERATION MUST BE LESS THAN OR EQUAL TO 4 BITS!" + converted2);
						}
						bits += converted2;
						bits += s2[4];
					}
					else if(s2.Length == 4)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length < 4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  SOURCE REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!" + converted1);
						}
						bits += converted1;
						converted1 = Convert.ToString(int.Parse(s2[2]), 2);
						if(converted1.Length < 4)
						{
							converted1 = converted1.PadLeft(4, '0');
						}
						if(converted1.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  TARGET REGISTER MUST BE LESS THAN OR EQUAL TO 4 BITS!" + converted1);
						}
						bits += converted1;
						string converted2 = Convert.ToString(int.Parse(s2[3]), 2);
						if(converted2.Length < 4)
						{
							converted2 = converted2.PadLeft(4, '0');
						}
						if(converted2.Length != 4)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  OPERATION MUST BE LESS THAN OR EQUAL TO 4 BITS!" + converted2);
						}
						bits += converted2;
						bits += "".PadLeft(16, '0');
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				else if(s.StartsWith("UTL"))
				{
					Except("AT LINE " + s + " THERE WAS AN ERROR!  MANUAL UTL CALLS ARE NOT YET IMPLEMENTED!");
				}
				else if(s.StartsWith("OFST"))
				{
					bits += "10110000"; //UTLC-OP00: OFFSET.
					List<string> s22 = new List<string>(s.Split(' '));
					int remCA = -1;
					for(int i = 0; i < s22.Count; i++)
					{
						if(s22[i].StartsWith("//"))
						{
							remCA = i;
						}
					}
					string[] s2 = s22.ToArray();
					if(remCA != -1)
					{
						s22.RemoveRange(remCA, s22.Count - remCA);
					}
					s2 = s22.ToArray();
					if(s2.Length == 2)
					{
						string converted1 = Convert.ToString(int.Parse(s2[1]), 2);
						if(converted1.Length < 24)
						{
							converted1 = converted1.PadLeft(24, '0');
						}
						if(converted1.Length != 24)
						{
							Except("AT LINE " + s + " THERE WAS AN ERROR!  ADDRESS MUST BE LESS THAN OR EQUAL TO 24 BITS!" + converted1);
						}
						bits += converted1;
					}
					else
					{
						Except("AT LINE " + s + " THERE WAS AN ERROR!  ARGUMENT NUMBER WRONG!");
					}
				}
				Except(s+"\n"+bits);
				compiled.Add(Convert.ToInt32(bits, 2));
			}
		}
		return compiled.ToArray();
	}

	void Update()
	{
		if(!Cursor.visible) //If your cursor is locked, enables mouselook.
		{
			if((!mouseInUse || Input.GetButton("Look")))
			{
				mouseX += Input.GetAxis("Mouse X") * lookSpeed * Time.deltaTime;
				mouseY -= Input.GetAxis("Mouse Y") * lookSpeed * Time.deltaTime;
			}
			else if(mouseInUse)
			{
				mouseInUse.accumulated.x += Input.GetAxis("Mouse X") * lookSpeed * Time.deltaTime;
				mouseInUse.accumulated.y -= Input.GetAxis("Mouse Y") * lookSpeed * Time.deltaTime;
				Cursor.visible = false;
				Cursor.lockState = CursorLockMode.Locked;
			}
		}
		mouseX = Mathf.Clamp(mouseX, -135f, 135f);
		mouseY = Mathf.Clamp(mouseY, -90f, 90f);
		Quaternion q = Quaternion.identity;
		q.eulerAngles = new Vector3(mouseY, mouseX, 0);
		transform.localRotation = q;
		if(Input.GetButtonUp("Interact") && !keyboardInUse && !Cursor.visible) //If your cusor is locked & you're not using a keyboard, interact with stuff on F.
		{
			Ray ray = new Ray(transform.position, transform.forward);
			RaycastHit rh;
			if(Physics.Raycast(ray, out rh, 5))
			{
				if(rh.collider.GetComponent<Computer>() != null)
				{
					rh.collider.GetComponent<Computer>().TogglePower();
				}
				if(rh.collider.GetComponent<PortKeyboard>() != null)
				{
					keyboardInUse = rh.collider.GetComponent<PortKeyboard>();
				}
				if(rh.collider.GetComponent<PortMouse>() != null)
				{
					mouseInUse = rh.collider.GetComponent<PortMouse>();
				}
			}
		}
		if(Input.GetButtonUp("Free") && keyboardInUse && !mouseInUse)
		{
			keyboardInUse = null;
		}
		else if(Input.GetButtonUp("Free") && mouseInUse)
		{
			mouseInUse = null;
		}
		if(Input.GetButtonUp("Fire1")) //Select things by looking at them & LMB-clicking when your cursor isn't visible.
		{
			if(!Cursor.visible && (!mouseInUse || Input.GetButton("Look")))
			{
				Ray ray = new Ray(transform.position, transform.forward);
				RaycastHit rh;
				if(Physics.Raycast(ray, out rh, 5))
				{
					//if()
					{
						selected = rh.collider.gameObject;
					}
				}
				else
				{
					selected = null;
				}
			}
		}
		keyboardO = Input.anyKeyDown;
		if(keyboardInUse && keyboardO)
		{
			//keyboardO = true;
			for(int i = 0; i < keyboardInUse.keyState.Length; i++)
			{
				keyboardInUse.keyState[i] = false;
			}
			foreach(char c in Input.inputString)
			{
				keyboardInUse.keyState[(int)c] = true;
				keyboardInUse.lastChars.Add((int)c);
			}
		}
		/*else if(keyboardInUse && keyboardO)
		{
			keyboardO = false;
		}*/
		if(Input.GetButtonUp("Fire2") && (!mouseInUse || Input.GetButton("Look"))) //Lock and unlock your cursor with RMB.
		{
			if(Cursor.visible)
			{
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.visible = false;
			}
			else
			{
				Cursor.lockState = CursorLockMode.None;
				Cursor.visible = true;
			}
		}
		if(mouseInUse && !Input.GetButton("Look"))
		{
			mouseInUse.buttonState[0] = Input.GetButton("Fire1");
			mouseInUse.buttonState[1] = Input.GetButton("Fire2");
			mouseInUse.buttonState[2] = Input.GetButton("Fire3");
		}
	}
}