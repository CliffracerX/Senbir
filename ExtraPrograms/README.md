# Senbir: Extra Programs
This folder is dedicated to some of the programs built into Senbir, and that I wrote during testing.

## BlinkenLightROM
Just a simple one-pixel flash in the upper left, designed to live in ROM.
 * Reqs: Default difficulty or easier

## Blinkenlights
The same as BlinkenLightROM, but designed to be loaded with the included bootloader.
 * Reqs: Default difficulty or easier

## Bootloader
The (admittedly outdated & a bit inefficient) bootloader program I wrote.
 * Reqs: Default difficulty or easier
 * Designed for THIRTY-TWO memory addresses.  Modification may be required to upgrade to higher counts.

## DrawWin
The program loaded on the drive that draws the win screen.
 * Reqs: Default difficulty or easier

## Paint
A paint program.  WASD to move the cursor, spacebar to toggle if the brush is "down" or not, and Q/E to cycle through the colors.
 * Reqs: Extended difficulty or easier

## GameTest
The Paint program was built from this - it's basically just a pixel you can move around with WASD.  Feels very reminiscent of one of Stro Bro's ye-olde Videlectrix titles.
 * Reqs: Extended difficulty or easier